import { BaseComponent } from '../../components/baseComponent';
import { Button } from '../../components/buttonComponent';

const { I } = inject();

class DynamicPropertiesPage {
  protected WAIT_TIME_IN_SECONDS: number = 5;

  dynamicPropertiesCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Dynamic Properties")]'
  );
  dynamicPropertiesRandomIdText: BaseComponent = new BaseComponent(
    '//p[contains(text(), "This text has random Id")]'
  );
  dynamicPropertiesWillEnableButtonEnabled: BaseComponent = new BaseComponent(
    '#enableAfter[type]:enabled'
  );
  dynamicPropertiesWillEnableButtonDisabled: BaseComponent = new BaseComponent(
    '#enableAfter[type]:disabled'
  );
  dynamicPropertiesColorChangeButton: Button = new Button('#colorChange');
  dynamicPropertiesColorChangeRedButton: Button = new Button(
    '#colorChange.text-danger'
  );
  dynamicPropertiesVisibleAfterButton: Button = new Button('#visibleAfter');

  openDynamicProperties() {
    this.dynamicPropertiesCategory.clickElement();
  }

  async validatePageDynamicElements() {
    for (let i = 0; i <= 1; i++) {
      this.dynamicPropertiesWillEnableButtonDisabled.waitForVisible();
      this.dynamicPropertiesColorChangeButton.waitForVisible();
      this.dynamicPropertiesVisibleAfterButton.dontSeeElement();
      this.dynamicPropertiesColorChangeRedButton.dontSeeElement();
      this.dynamicPropertiesWillEnableButtonEnabled.dontSeeElement();
      I.wait(this.WAIT_TIME_IN_SECONDS);
      this.dynamicPropertiesWillEnableButtonEnabled.seeElement();
      this.dynamicPropertiesVisibleAfterButton.seeElement();
      this.dynamicPropertiesColorChangeRedButton.seeElement();
      this.dynamicPropertiesWillEnableButtonDisabled.dontSeeElement();
      I.refreshPage();
    }
  }
}

export = new DynamicPropertiesPage();
