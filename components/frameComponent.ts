import { BaseComponent } from './baseComponent';

const { I } = inject();

export class FrameComponent extends BaseComponent {
  constructor(selector: string) {
    super(selector);
  }

  switchBetweenFrames() {
    I.waitForVisible(this.getSelector());
    I.switchTo(this.getSelector());
  }

  async getAllObjectsAsArray(): Promise<FrameComponent[]> {
    return await this.getAllObjectsAsSameType<FrameComponent>(FrameComponent);
  }
}
