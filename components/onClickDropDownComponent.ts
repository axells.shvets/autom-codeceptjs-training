import { BaseComponent } from './baseComponent';

const { I } = inject();

export class onClickDropDownComponent extends BaseComponent {
  protected interactiveDropDown: string;
  protected item: string;

  constructor(selector: string, interactiveDropDown: string, item?: string) {
    super(selector);
    this.interactiveDropDown = interactiveDropDown;
    this.item = item;
  }

  selectInteractiveDropDownOption(item?: string) {
    I.waitForVisible(this.getSelector());
    I.click(this.getSelector());
    I.waitForVisible(this.interactiveDropDown);
    I.click(item || this.item);
  }

  seeTextInField(text: string) {
    I.waitForVisible(this.getSelector());
    I.seeInField(this.getSelector(), text);
  }
}
