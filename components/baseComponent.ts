const { I } = inject();

export class BaseComponent {
  protected selector: string;

  constructor(selector: string) {
    this.selector = selector;
  }

  getSelector(): string {
    return this.selector;
  }

  waitForVisible(sec: number = 30) {
    I.waitForVisible(this.getSelector(), sec);
  }

  waitForElementOnPage(sec: number = 30) {
    I.waitForElement(this.getSelector(), sec);
  }

  waitToHide(sec: number = 30) {
    I.waitToHide(this.getSelector(), sec);
  }

  seeElement() {
    I.seeElement(this.getSelector());
  }

  dontSeeElement() {
    I.dontSeeElement(this.getSelector());
  }

  waitForTextOnElement(text: string, waitTime?: number) {
    I.waitForText(text, waitTime, this.getSelector());
  }

  scrollTo() {
    I.waitForVisible(this.getSelector());
    I.scrollTo(this.getSelector());
  }

  async grabText(): Promise<string> {
    I.waitForVisible(this.getSelector());
    return await I.grabTextFrom(this.getSelector());
  }

  async getTextValue(): Promise<string> {
    return this.grabText()
      .then((value) => {
        return value;
      })
      .catch((err) => {
        return err;
      });
  }

  async grabTextFromAllElements(): Promise<string[]> {
    I.waitForElement(this.getSelector());
    return await I.grabTextFromAll(this.getSelector());
  }

  async grabAttribute(attr: string): Promise<string> {
    I.waitForElement(this.getSelector());
    return await I.grabAttributeFrom(this.getSelector(), attr);
  }

  async grabNumberOfElements(): Promise<number> {
    I.waitForVisible(this.getSelector());
    return await I.grabNumberOfVisibleElements(this.getSelector());
  }

  hoverElement() {
    I.waitForVisible(this.getSelector());
    I.moveCursorTo(this.getSelector());
    I.wait(1);
  }

  clickByJS() {
    I.waitForElement(this.getSelector());
    I.forceClick(this.getSelector());
  }

  clickElement() {
    I.waitForElement(this.getSelector());
    I.click(this.getSelector());
  }

  checkElementAttribute(attr: object) {
    I.waitForElement(this.getSelector());
    I.seeAttributesOnElements(this.getSelector(), attr);
  }

  async getElementSize(): Promise<DOMRect | number> {
    I.waitForElement(this.getSelector());
    return await I.grabElementBoundingRect(this.getSelector());
  }

  async getElementWidth(): Promise<DOMRect | number> {
    I.waitForElement(this.getSelector());
    return await I.grabElementBoundingRect(this.getSelector(), 'width');
  }

  async getElementHeight(): Promise<DOMRect | number> {
    I.waitForElement(this.getSelector());
    return await I.grabElementBoundingRect(this.getSelector(), 'height');
  }

  async getObjectsCount(selector = this.getSelector()): Promise<number> {
    try {
      return I.grabNumberOfVisibleElements(selector);
    } catch (e) {
      console.log(e);
    }
    return 0;
  }

  async getAllObjectsAsSameType<T>(
    objType,
    replaceNumber = false
  ): Promise<T[]> {
    let curObj = 2;
    let selector = this.getSelector();

    if (replaceNumber) {
      curObj = 1;
      const regexNumber = /\[\d{1,2}\]$/gm;
      if (selector.match(regexNumber)) {
        selector = selector.replace(regexNumber, '');
      }
    }

    const res: any[] = replaceNumber ? [] : [this];
    let count = 0;
    try {
      count = await this.getObjectsCount(selector);
    } catch (e) {
      console.log(`getAllObjectsAsSameType: element not found: ${selector}`);
    }
    for (; curObj <= count; ++curObj) {
      const curSelector = `${selector}[${curObj}]`;
      res.push(new objType(curSelector));
    }
    return res;
  }

  async getAllObjectsAsArray(): Promise<BaseComponent[]> {
    return await this.getAllObjectsAsSameType<BaseComponent>(BaseComponent);
  }

  transFormComponent(component: BaseComponent, targetComponent) {
    return new targetComponent(component.getSelector());
  }
}
