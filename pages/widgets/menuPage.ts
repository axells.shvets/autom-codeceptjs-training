import { BaseComponent } from '../../components/baseComponent';

const { I } = inject();

class MenuPage {
  menuCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Menu")]'
  );
  menuMainItemsAll: BaseComponent = new BaseComponent('//*[@id="nav"]/li');
  menuItemTwoAll: BaseComponent = new BaseComponent('//*[@id="nav"]/li/ul/li');
  menuItemTwoSubMenuAll: BaseComponent = new BaseComponent('//li[3]/ul/li');

  openMenuCategory() {
    this.menuCategory.clickElement();
  }

  async hoverAllMenuItems() {
    I.wait(1);
    let allMenus = await this.menuMainItemsAll.getAllObjectsAsArray();
    for (let i = 0; i < allMenus.length; i++) {
      allMenus[i].hoverElement();
      if (i === 1) {
        let allMenuTwoItems = await this.menuItemTwoAll.getAllObjectsAsArray();
        for (let l = 0; l < allMenuTwoItems.length; l++) {
          allMenuTwoItems[l].hoverElement();
          if (l === 2) {
            let allMenuTwoSubMenus =
              await this.menuItemTwoSubMenuAll.getAllObjectsAsArray();
            for (let submenu of allMenuTwoSubMenus) {
              submenu.hoverElement();
            }
          }
        }
      }
    }
  }
}

export = new MenuPage();
