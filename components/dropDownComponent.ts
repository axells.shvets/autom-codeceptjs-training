import { BaseComponent } from './baseComponent';

const { I } = inject();

export class DropDown extends BaseComponent {
  protected item: string;

  constructor(selector: string, item?: string) {
    super(selector);
    this.item = item;
  }

  selectSelectOption(option: string) {
    I.waitForVisible(this.getSelector());
    I.selectOption(this.getSelector(), option);
  }

  async getAllSelectOptionsText(): Promise<string[]> {
    const selector = `${this.getSelector()}${this.item}`;
    return await I.grabTextFromAll(selector);
  }

  async getAllObjectsAsArray(): Promise<DropDown[]> {
    return await this.getAllObjectsAsSameType<DropDown>(DropDown);
  }
}
