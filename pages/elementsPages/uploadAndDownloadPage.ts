import { BaseComponent } from '../../components/baseComponent';
import { Button } from '../../components/buttonComponent';
import { SelectUploadFile } from '../../components/selectUploadFileComponent';

const { I } = inject();

class UploadAndDownloadPage {
  protected fileName: string =
    Math.floor(Date.now() / 1000).toString() + '.jpg';
  protected donwloandPath: string = './output/downloads';
  protected downloadFileName: string = 'downloads/' + this.fileName;
  protected uploadFilePath: string = './data/testData.js';

  uploadAndDownloadCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Upload and Download")]'
  );
  uploadAndDownloadDownloadButton: Button = new Button('#downloadButton');
  uploadAndDownloadSelectFile: SelectUploadFile = new SelectUploadFile(
    '#uploadFile'
  );
  uploadAndDownloadFileAcceptedMessage: BaseComponent = new BaseComponent(
    '//p[@id="uploadedFilePath"][contains(text(), "testData.js")]'
  );

  openUploadAndDownloadCategory() {
    this.uploadAndDownloadCategory.clickElement();
  }

  downloadSampleFile() {
    I.handleDownloads(this.downloadFileName);
    this.uploadAndDownloadDownloadButton.clickButton();
    I.amInPath(this.donwloandPath);
    I.waitForFile(this.fileName, 10);
  }

  uploadSampleFile() {
    this.uploadAndDownloadSelectFile.selectFileToAttach(this.uploadFilePath);
    this.uploadAndDownloadFileAcceptedMessage.waitForVisible();
  }
}

export = new UploadAndDownloadPage();
