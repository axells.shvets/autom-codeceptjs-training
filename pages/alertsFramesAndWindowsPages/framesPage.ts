import { BaseComponent } from '../../components/baseComponent';
import { FrameComponent } from '../../components/frameComponent';

const { I } = inject();

class FramesPage {
  framesCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Frames")]'
  );
  framesFirstIframe: FrameComponent = new FrameComponent('#frame1');
  framesSecondIframe: FrameComponent = new FrameComponent('#frame2');
  framesFirstIframeHeading: BaseComponent = new BaseComponent('#sampleHeading');
  framesSecondIframeHeading: BaseComponent = new BaseComponent(
    '#sampleHeading'
  );

  openFramesCategory() {
    this.framesCategory.clickElement();
  }

  switchBetweenIframes() {
    this.framesFirstIframe.switchBetweenFrames();
    this.framesFirstIframeHeading.waitForVisible();
    this.framesFirstIframe.waitToHide();
    I.switchTo();
    this.framesSecondIframe.switchBetweenFrames();
    this.framesSecondIframeHeading.waitForVisible();
    this.framesFirstIframe.waitToHide();
  }
}

export = new FramesPage();
