import { BaseComponent } from './baseComponent';

const { I } = inject();

export class EditBox extends BaseComponent {
  constructor(selector: string) {
    super(selector);
  }

  clearText() {
    I.waitForElement(this.getSelector());
    I.clearField(this.getSelector());
  }

  fillFieldWithNewText(text: string | number) {
    I.waitForElement(this.getSelector());
    I.fillField(this.getSelector(), text);
  }

  addTextToField(text: string) {
    I.waitForElement(this.getSelector());
    I.appendField(this.getSelector(), text);
  }

  typeText(text: string) {
    I.waitForVisible(this.getSelector());
    I.type(text);
  }

  seeTextInField(text: string) {
    I.waitForVisible(this.getSelector());
    I.seeInField(this.getSelector(), text);
  }

  async getAllObjectsAsArray(): Promise<EditBox[]> {
    return await this.getAllObjectsAsSameType<EditBox>(EditBox);
  }
}
