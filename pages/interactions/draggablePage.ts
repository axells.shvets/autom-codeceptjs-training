import assert from 'assert';
import { BaseComponent } from '../../components/baseComponent';
import { DraggableComponent } from '../../components/draggableComponent';

const { I } = inject();

class DraggablePage {
  protected draggableDirections: string[] = [
    '#draggableExample-tab-simple',
    '#draggableExample-tab-axisRestriction',
    '//span[contains(text(),"Selectable")]',
  ];

  draggableCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Dragabble")]'
  );
  draggableSimpleTab: BaseComponent = new BaseComponent(
    '#draggableExample-tab-simple'
  );
  draggableAxisRestrictedTab: BaseComponent = new BaseComponent(
    '#draggableExample-tab-axisRestriction'
  );
  draggableContainerRestrictedTab: BaseComponent = new BaseComponent(
    '#draggableExample-tab-containerRestriction'
  );
  draggableCursorStyleTab: BaseComponent = new BaseComponent(
    '#draggableExample-tab-cursorStyle'
  );
  draggableSimpleDragMeDragg: DraggableComponent = new DraggableComponent(
    '#dragBox'
  );
  draggableAxisRestrictedOnlyXDragg: DraggableComponent =
    new DraggableComponent('#restrictedX');
  draggableAxisRestrictedOnlyYDragg: DraggableComponent =
    new DraggableComponent('#restrictedY');
  draggableContainerRestrictedContainWithinBoxDragg: DraggableComponent =
    new DraggableComponent('#containmentWrapper > div');
  draggableContainerRestrictedCotainWithinParrentDragg: DraggableComponent =
    new DraggableComponent('div.draggable.ui-widget-content.m-3 > span');
  draggableCursorStyleStickToCenterDragg: DraggableComponent =
    new DraggableComponent('#cursorCenter');
  draggableCursorStyleCursorTopLeftDragg: DraggableComponent =
    new DraggableComponent('#cursorTopLeft');
  draggableCursorStyleCursorIsBottomDragg: DraggableComponent =
    new DraggableComponent('#cursorBottom');

  openDraggableCategory() {
    this.draggableCategory.clickElement();
  }

  simpleCategoryValidation() {
    this.draggableSimpleTab.clickElement();
    this.draggableSimpleDragMeDragg.waitForVisible();
    for (let i = 0; i < this.draggableDirections.length; i++) {
      this.draggableSimpleDragMeDragg.dragAndDropElement(
        this.draggableDirections[i]
      );
    }
  }

  axisRestrictedValidation() {
    this.draggableAxisRestrictedTab.clickElement();
    this.draggableAxisRestrictedOnlyXDragg.waitForVisible();
    this.draggableAxisRestrictedOnlyYDragg.waitForVisible();
    for (let i = 0; i < this.draggableDirections.length; i++) {
      this.draggableAxisRestrictedOnlyXDragg.dragAndDropElement(
        this.draggableDirections[i]
      );
      this.draggableAxisRestrictedOnlyYDragg.dragAndDropElement(
        this.draggableDirections[i]
      );
    }
  }

  containerRestrictedValidation() {
    this.draggableContainerRestrictedTab.clickElement();
    this.draggableContainerRestrictedContainWithinBoxDragg.waitForVisible();
    this.draggableContainerRestrictedCotainWithinParrentDragg.waitForVisible();
    for (let i = 0; i < this.draggableDirections.length; i++) {
      this.draggableContainerRestrictedContainWithinBoxDragg.dragAndDropElement(
        this.draggableDirections[i]
      );
      this.draggableContainerRestrictedCotainWithinParrentDragg.dragAndDropElement(
        this.draggableDirections[i]
      );
    }
  }

  cursorStileValidation() {
    this.draggableCursorStyleTab.clickElement();
    this.draggableCursorStyleStickToCenterDragg.waitForVisible();
    this.draggableCursorStyleCursorTopLeftDragg.waitForVisible();
    this.draggableCursorStyleCursorIsBottomDragg.waitForVisible();
    for (let i = 0; i < this.draggableDirections.length; i++) {
      this.draggableCursorStyleStickToCenterDragg.dragAndDropElement(
        this.draggableDirections[i]
      );
      this.draggableCursorStyleCursorTopLeftDragg.dragAndDropElement(
        this.draggableDirections[i]
      );
      this.draggableCursorStyleCursorIsBottomDragg.dragAndDropElement(
        this.draggableDirections[i]
      );
    }
  }
}

export = new DraggablePage();
