import { BaseComponent } from '../../components/baseComponent';

const { I } = inject();

class LinksPage {
  protected elementsLinkUrl: string = 'https://demoqa.com/';
  protected elementsHttpRequestMethod: string = 'GET';
  protected linksCreatedLink: string = '#created';
  protected linksNoContentLink: string = '#no-content';
  protected linksMovedLink: string = '#moved';
  protected linksBadRequestLink: string = '#bad-request';
  protected linksUnauthorizedLink: string = '#unauthorized';
  protected linksForbiddenLink: string = '#forbidden';
  protected linksNotFoundLink: string = '#invalid-url';

  linksCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Links")]'
  );
  linksHomeLink: BaseComponent = new BaseComponent('#simpleLink');
  linksHomeRg2FLink: BaseComponent = new BaseComponent('#dynamicLink');

  linksAllRequestLinks: any[] = [
    [this.linksCreatedLink, 'https://demoqa.com/created', 201],
    [this.linksNoContentLink, 'https://demoqa.com/no-content', 204],
    [this.linksMovedLink, 'https://demoqa.com/moved', 301],
    [this.linksBadRequestLink, 'https://demoqa.com/bad-request', 400],
    [this.linksUnauthorizedLink, 'https://demoqa.com/unauthorized', 401],
    [this.linksForbiddenLink, 'https://demoqa.com/forbidden', 403],
    [this.linksNotFoundLink, 'https://demoqa.com/invalid-url', 404],
  ];

  openLinksCategory() {
    this.linksCategory.clickElement();
  }

  async clickAndValidateAllPageLinks() {
    this.linksHomeLink.clickElement();
    I.retry().switchToNextTab(1);
    I.seeCurrentUrlEquals(this.elementsLinkUrl);
    I.switchToPreviousTab();
    I.closeOtherTabs();
    this.linksHomeRg2FLink.clickElement();
    I.retry().switchToNextTab(1);
    I.seeCurrentUrlEquals(this.elementsLinkUrl);
    I.switchToPreviousTab();
    I.closeOtherTabs();
    // Using direct playwright code as codeceptjs standard 'I.waitForRequest' and 'I.waitForResponse' are not working
    for (let i = 0; i < this.linksAllRequestLinks.length; i++) {
      I.usePlaywrightTo(
        'validating http request and response as standard codeceptjs helpers for this purposes are not working',
        async ({ page }) => {
          await Promise.all([
            page.waitForRequest(
              (request) =>
                request.url() === this.linksAllRequestLinks[i][1] &&
                request.method() === this.elementsHttpRequestMethod
            ),
            page.waitForResponse(
              (response) =>
                response.url() === this.linksAllRequestLinks[i][1] &&
                response.status() === this.linksAllRequestLinks[i][2]
            ),
            page.click(this.linksAllRequestLinks[i][0]),
          ]);
        }
      );
    }
  }
}

export = new LinksPage();
