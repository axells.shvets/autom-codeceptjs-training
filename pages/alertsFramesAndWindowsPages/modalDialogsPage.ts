import { BaseComponent } from '../../components/baseComponent';
import { Button } from '../../components/buttonComponent';

const { I } = inject();

class ModalDialogsPage {
  modalDialogsCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Modal Dialogs")]'
  );
  modalDialogsSmallModalButton: Button = new Button('#showSmallModal');
  modalDialogsLargeModalButton: Button = new Button('#showLargeModal');
  modalDialogsSmallModalCloseButton: Button = new Button('#closeSmallModal');
  modalDialogsLargeModalCloseButton: Button = new Button('#closeLargeModal');
  modalDialogsCloseIcon: Button = new Button('div.modal-header > button');
  modalDialogsAllModalElements: Object[] = [
    [
      this.modalDialogsSmallModalButton,
      this.modalDialogsSmallModalCloseButton,
      this.modalDialogsCloseIcon,
    ],
    [
      this.modalDialogsLargeModalButton,
      this.modalDialogsLargeModalCloseButton,
      this.modalDialogsCloseIcon,
    ],
  ];

  openModalDialogsCategory() {
    this.modalDialogsCategory.clickElement();
  }

  openAndCloseAllModals() {
    for (let i = 0; i < this.modalDialogsAllModalElements.length; i++) {
      this.modalDialogsAllModalElements[i][0].clickButton();
      this.modalDialogsAllModalElements[i][1].clickButton();
      this.modalDialogsAllModalElements[i][0].waitForVisible();
    }
  }
  openAndCloseAllModalsViaIcon() {
    for (let i = 0; i < this.modalDialogsAllModalElements.length; i++) {
      this.modalDialogsAllModalElements[i][0].clickButton();
      this.modalDialogsAllModalElements[i][2].clickButton();
      this.modalDialogsAllModalElements[i][0].waitForVisible();
    }
  }
}

export = new ModalDialogsPage();
