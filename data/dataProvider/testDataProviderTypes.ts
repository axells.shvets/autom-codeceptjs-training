export type dataSingle = {
  firstName?: string;
  lastName?: string;
  email?: string;
  age?: string;
  salary?: string;
  department?: string;
  currentAddress?: string;
  permanentAddress?: string;
  editText?: string;
  mobile?: string;
  state?: string;
  city?: string;
  subjects?: any[];
};

export type dataSingleDateAndTime = {
  yearsList?: any[];
  monthsList?: any[];
  daysList?: any[];
  timeValuesList?: any[];
};
