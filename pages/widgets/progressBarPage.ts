import assert from 'assert';
import { BaseComponent } from '../../components/baseComponent';
import { Button } from '../../components/buttonComponent';

const { I } = inject();

class ProgressBarPage {
  protected progressBarStart = '0';
  protected progressBarEnd = '100';
  protected progressBarSliderValueNow: string = 'aria-valuenow';

  progressBarCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Progress Bar")]'
  );
  progressBarStartButton: Button = new Button('#startStopButton');
  progressBarSlider: BaseComponent = new BaseComponent('#progressBar > div');
  progressBarResetButton: Button = new Button('#resetButton');

  openProgressBarCategory() {
    this.progressBarCategory.clickElement();
  }

  async startAndResetProgressBar() {
    this.progressBarSlider.waitForElementOnPage();
    let start = await this.progressBarSlider.grabAttribute(
      this.progressBarSliderValueNow
    );
    assert(start === this.progressBarStart);
    this.progressBarStartButton.clickButton();
    this.progressBarResetButton.waitForVisible();
    I.wait(0.1);
    let end = await this.progressBarSlider.grabAttribute(
      this.progressBarSliderValueNow
    );
    assert(end === this.progressBarEnd);
    this.progressBarStartButton.waitToHide();
    this.progressBarResetButton.clickButton();
    this.progressBarResetButton.waitToHide();
    this.progressBarStartButton.clickButton();
    I.wait(1);
    this.progressBarStartButton.clickButton();
    let progress = await this.progressBarSlider.grabAttribute(
      this.progressBarSliderValueNow
    );
    assert(
      progress !== this.progressBarStart && progress !== this.progressBarEnd
    );
  }
}

export = new ProgressBarPage();
