import assert from 'assert';
import { BaseComponent } from '../../components/baseComponent';
import { DraggableComponent } from '../../components/draggableComponent';

const { I } = inject();

class DroppablePage {
  protected droppableDroppedText: string = 'Dropped!';

  droppableCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Droppable")]'
  );

  //Simple Tab
  droppableSimpleTab: BaseComponent = new BaseComponent(
    '#droppableExample-tab-simple'
  );
  droppableSimpleDragMeElement: DraggableComponent = new DraggableComponent(
    '#draggable',
    '//div[@id="droppableExample-tabpane-simple"]//div[@id="droppable"]'
  );
  droppableSimpleTargetArea: BaseComponent = new BaseComponent(
    '//div[@id="droppableExample-tabpane-simple"]//div[@id="droppable"]/p'
  );

  //Accept Tab
  droppableAcceptTab: BaseComponent = new BaseComponent(
    '#droppableExample-tab-accept'
  );
  droppableAcceptNotAcceptableElement: DraggableComponent =
    new DraggableComponent(
      '#notAcceptable',
      '//div[@id="droppableExample-tabpane-accept"]//div[@id="droppable"]'
    );
  droppableAcceptAcceptableElement: DraggableComponent = new DraggableComponent(
    '#acceptable',
    '//div[@id="droppableExample-tabpane-accept"]//div[@id="droppable"]'
  );
  droppableAcceptTargetArea: BaseComponent = new BaseComponent(
    '//div[@id="droppableExample-tabpane-accept"]//div[@id="droppable"]/p'
  );

  //Prevent Propogation Tab
  protected droppablePreventPropagationNotGreedyTarget: string =
    '#notGreedyInnerDropBox';
  protected droppablePreventPropagationGreedyTarget: string =
    '#greedyDropBoxInner';
  droppablePreventPropogationTab: BaseComponent = new BaseComponent(
    '#droppableExample-tab-preventPropogation'
  );
  droppablePreventPropagationDragMeElement: DraggableComponent =
    new DraggableComponent('#dragBox');
  droppablePreventPropagationNotGreedyDropBoxText: BaseComponent =
    new BaseComponent('#notGreedyDropBox > p');
  droppablePreventPropagationGreedyDropBoxText: BaseComponent =
    new BaseComponent('#greedyDropBox > p');

  //Revert Draggable Tab
  droppableRevertDraggableTab: BaseComponent = new BaseComponent(
    '#droppableExample-tab-revertable'
  );
  droppableRevertDraggableRevertableElement: DraggableComponent =
    new DraggableComponent(
      '#revertable',
      '//div[@id="droppableExample-tabpane-revertable"]//div[@id="droppable"]'
    );
  droppableRevertDraggableNotRevertableElement: DraggableComponent =
    new DraggableComponent(
      '#notRevertable',
      '//div[@id="droppableExample-tabpane-revertable"]//div[@id="droppable"]'
    );
  droppableRevertDraggableTargetAreaText: BaseComponent = new BaseComponent(
    '//div[@id="droppableExample-tabpane-revertable"]//div[@id="droppable"]/p'
  );

  openDroppableCategory() {
    this.droppableCategory.clickElement();
  }

  dragAndDropSimpleTab() {
    this.droppableSimpleTab.clickElement();
    this.droppableSimpleDragMeElement.dragAndDropElement();
    this.droppableSimpleTargetArea.waitForTextOnElement(
      this.droppableDroppedText
    );
  }

  async dragAndDropAcceptTab() {
    this.droppableAcceptTab.clickElement();
    this.droppableAcceptNotAcceptableElement.dragAndDropElement();
    assert(
      this.droppableDroppedText !==
        (await this.droppableAcceptTargetArea.grabText())
    );
    this.droppableAcceptAcceptableElement.dragAndDropElement();
    assert(
      this.droppableDroppedText ===
        (await this.droppableAcceptTargetArea.grabText())
    );
  }

  async dragAndDropPreventPropagationTab() {
    this.droppablePreventPropogationTab.clickElement();
    this.droppablePreventPropagationDragMeElement.dragAndDropElement(
      this.droppablePreventPropagationNotGreedyTarget
    );
    assert(
      this.droppableDroppedText ===
        (await this.droppablePreventPropagationNotGreedyDropBoxText.grabText())
    );
    this.droppablePreventPropagationDragMeElement.dragAndDropElement(
      this.droppablePreventPropagationGreedyTarget
    );
    assert(
      this.droppableDroppedText !==
        (await this.droppablePreventPropagationGreedyDropBoxText.grabText())
    );
  }

  async dragAndDropRevertDraggableTab() {
    this.droppableRevertDraggableTab.clickElement();
    this.droppableRevertDraggableNotRevertableElement.dragAndDropElement();
    this.droppableRevertDraggableRevertableElement.dragAndDropElement(
      this.droppableRevertDraggableNotRevertableElement.getSelector()
    );
    assert(
      this.droppableDroppedText ===
        (await this.droppableRevertDraggableTargetAreaText.grabText())
    );
  }
}

export = new DroppablePage();
