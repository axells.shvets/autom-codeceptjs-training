/// <reference types='codeceptjs' />
type landingPage = typeof import('./pages/landingPage');
type elementsTextBoxPage = typeof import('./pages/elementsPages/textBoxPage');
type elementsCheckBoxPage = typeof import('./pages/elementsPages/checkBoxPage');
type elementsRadioButtonPage =
  typeof import('./pages/elementsPages/radioButtonPage');
type elementsWebTablesPage =
  typeof import('./pages/elementsPages/webTablesPage');
type elementsButtonsPage = typeof import('./pages/elementsPages/buttonsPage');
type elementsLinksPage = typeof import('./pages/elementsPages/linksPage');
type elementsBrokenLinksPage =
  typeof import('./pages/elementsPages/brokenLinksPage');
type elementsUploadsAndDownloadsPage =
  typeof import('./pages/elementsPages/uploadAndDownloadPage');
type elementsDynamicPropertiesPage =
  typeof import('./pages/elementsPages/dynamicPropertiesPage');
type formsPracticeFormPage =
  typeof import('./pages/formsPages/practiceFormPage');
type alertsFramesAndWindowsBrowserWindowsPage =
  typeof import('./pages/alertsFramesAndWindowsPages/browserWindowsPage');
type alertsFramesAndWindowsAlertsPage =
  typeof import('./pages/alertsFramesAndWindowsPages/alertsPage');
type alertsFramesAndWindowsFramesPage =
  typeof import('./pages/alertsFramesAndWindowsPages/framesPage');
type alertsFramesAndWindowsNestedFramesPage =
  typeof import('./pages/alertsFramesAndWindowsPages/nestedFramesPage');
type alertsFramesAndWindowsModalDialogsPage =
  typeof import('./pages/alertsFramesAndWindowsPages/modalDialogsPage');
type widgetsAccordianPage = typeof import('./pages/widgets/accordianPage');
type widgetsAutoCompletePage =
  typeof import('./pages/widgets/autoCompletePage');
type widgetsDatePickerPage = typeof import('./pages/widgets/datePickerPage');
type widgetsSliderPage = typeof import('./pages/widgets/sliderPage');
type widgetsProgressBarPage = typeof import('./pages/widgets/progressBarPage');
type widgetsTabsPage = typeof import('./pages/widgets/tabsPage');
type widgetsToolTipsPage = typeof import('./pages/widgets/toolTipsPage');
type widgetsMenuPage = typeof import('./pages/widgets/menuPage');
type widgetsSelectMenuPage = typeof import('./pages/widgets/selectMenuPage');
type interactionsSortablePage =
  typeof import('./pages/interactions/sortablePage');
type interactionsSelectablePage =
  typeof import('./pages/interactions/selectablePage');
type interactionsResizablePage =
  typeof import('./pages/interactions/resizablePage');
type interactionsDroppablePage =
  typeof import('./pages/interactions/droppablePage');
type interactionsDraggablePage =
  typeof import('./pages/interactions/draggablePage');

declare namespace CodeceptJS {
  interface SupportObject {
    I: I;
    current: any;
    landingPage: landingPage;
    elementsTextBoxPage: elementsTextBoxPage;
    elementsCheckBoxPage: elementsCheckBoxPage;
    elementsRadioButtonPage: elementsRadioButtonPage;
    elementsWebTablesPage: elementsWebTablesPage;
    elementsButtonsPage: elementsButtonsPage;
    elementsLinksPage: elementsLinksPage;
    elementsBrokenLinksPage: elementsBrokenLinksPage;
    elementsUploadsAndDownloadsPage: elementsUploadsAndDownloadsPage;
    elementsDynamicPropertiesPage: elementsDynamicPropertiesPage;
    formsPracticeFormPage: formsPracticeFormPage;
    alertsFramesAndWindowsBrowserWindowsPage: alertsFramesAndWindowsBrowserWindowsPage;
    alertsFramesAndWindowsAlertsPage: alertsFramesAndWindowsAlertsPage;
    alertsFramesAndWindowsFramesPage: alertsFramesAndWindowsFramesPage;
    alertsFramesAndWindowsNestedFramesPage: alertsFramesAndWindowsNestedFramesPage;
    alertsFramesAndWindowsModalDialogsPage: alertsFramesAndWindowsModalDialogsPage;
    widgetsAccordianPage: widgetsAccordianPage;
    widgetsAutoCompletePage: widgetsAutoCompletePage;
    widgetsDatePickerPage: widgetsDatePickerPage;
    widgetsSliderPage: widgetsSliderPage;
    widgetsProgressBarPage: widgetsProgressBarPage;
    widgetsTabsPage: widgetsTabsPage;
    widgetsToolTipsPage: widgetsToolTipsPage;
    widgetsMenuPage: widgetsMenuPage;
    widgetsSelectMenuPage: widgetsSelectMenuPage;
    interactionsSortablePage: interactionsSortablePage;
    interactionsSelectablePage: interactionsSelectablePage;
    interactionsResizablePage: interactionsResizablePage;
    interactionsDroppablePage: interactionsDroppablePage;
    interactionsDraggablePage: interactionsDraggablePage;
  }
  interface Methods extends Playwright, FileSystem, Mochawesome {}
  interface I extends WithTranslation<Methods> {}
  namespace Translation {
    interface Actions {}
  }
}
