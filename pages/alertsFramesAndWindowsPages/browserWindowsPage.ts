import { BaseComponent } from '../../components/baseComponent';
import { Button } from '../../components/buttonComponent';

const { I } = inject();

class BrowserWindowsPage {
  protected browserWindowsTabUrl: string = 'https://demoqa.com/sample';
  protected browserWindowsMessageWindowURL: string =
    'https://demoqa.com/browser-windows';

  browserWindowsCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Browser Windows")]'
  );
  browserWindowsNewTabButton: Button = new Button('#tabButton');
  browserWindowsNewWindowButton: Button = new Button('#windowButton');
  browserWindowsNewWindowMessageButton: Button = new Button(
    '#messageWindowButton'
  );
  browserWindowsAllWindows: any[] = [
    [this.browserWindowsNewTabButton, this.browserWindowsTabUrl],
    [this.browserWindowsNewWindowButton, this.browserWindowsTabUrl],
    [
      this.browserWindowsNewWindowMessageButton,
      this.browserWindowsMessageWindowURL,
    ],
  ];

  openBrowserWindowsCategory() {
    this.browserWindowsCategory.clickElement();
  }

  validateBrowserWindowsPageButtons() {
    for (let i = 0; i < this.browserWindowsAllWindows.length; i++) {
      this.browserWindowsAllWindows[i][0].clickButton();
      I.retry().switchToNextTab(1);
      I.seeCurrentUrlEquals(this.browserWindowsAllWindows[i][1]);
      I.switchToPreviousTab();
      I.closeOtherTabs();
    }
  }
}

export = new BrowserWindowsPage();
