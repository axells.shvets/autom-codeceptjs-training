const testData = function () {
  let testData = {};

  let webTablesData = new DataTable([
    'firstName',
    'lastName',
    'email',
    'age',
    'salary',
    'department',
  ]);
  webTablesData.add([
    'FirstName_01',
    'LastName_01',
    'test01@examlpe.com',
    '25',
    '300',
    'Quality Assurance',
  ]);
  webTablesData.add([
    'FirstName_02',
    'LastName_02',
    'test02@examlpe.com',
    '26',
    '300000',
    'Development',
  ]);
  webTablesData.add([
    'FirstName_03',
    'LastName_03',
    'test03@examlpe.com',
    '27',
    '30000000',
    'PMO Office',
  ]);
  webTablesData.add([
    'FirstName_04',
    'LastName_04',
    'test04@examlpe.com',
    '28',
    '300000000',
    'Chief Board Member',
  ]);
  webTablesData.add([
    'FirstName_05',
    'LastName_05',
    'test05@examlpe.com',
    '29',
    '3000000000',
    'Handy Man',
  ]);

  testData.webTablesData = webTablesData;

  return testData;
};

module.exports = testData;
