import assert from 'assert';
import { BaseComponent } from '../../components/baseComponent';
import { EditBox } from '../../components/editBoxComponent';

const { I } = inject();

class AutoCompletePage {
  protected autoCompleteSingleTextElement: string =
    'div.auto-complete__single-value.css-1uccc91-singleValue';

  autoCompleteCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Auto Complete")]'
  );
  autoCompleteMultipleField: EditBox = new EditBox(
    '#autoCompleteMultipleInput'
  );
  autoCompleteSingleField: EditBox = new EditBox('#autoCompleteSingleInput');

  openAutoCompleteCategory() {
    this.autoCompleteCategory.clickElement();
  }

  async fillInAndValidateAutocompleteMultipleField(colors: string[]) {
    for (let i = 0; i < colors.length; i++) {
      this.autoCompleteMultipleField.addTextToField(colors[i]);
      this.autoCompleteMultipleField.seeTextInField(colors[i]);
      I.pressKey('Enter');
      let pin = await I.grabTextFrom(
        `div:nth-child(${
          i + 1
        }) > div.css-12jo7m5.auto-complete__multi-value__label`
      );
      assert(pin === colors[i]);
    }
  }

  async fillInAndValidateAutocompleteSingle(colors: string[]) {
    for (let i = 0; i < colors.length; i++) {
      this.autoCompleteSingleField.addTextToField(colors[i]);
      this.autoCompleteSingleField.seeTextInField(colors[i]);
      I.pressKey('Enter');
      let pin = await I.grabTextFrom(this.autoCompleteSingleTextElement);
      assert(pin === colors[i]);
    }
  }
}

export = new AutoCompletePage();
