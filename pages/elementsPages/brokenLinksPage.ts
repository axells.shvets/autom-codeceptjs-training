import { BaseComponent } from '../../components/baseComponent';

const { I } = inject();

class BrokenLinksPage {
  protected currentUrlValidLink: string = 'https://demoqa.com/';
  protected currentUrlInvalidLink: string =
    'http://the-internet.herokuapp.com/status_codes/500';

  brokenLinksCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Broken Links")]'
  );
  brokenLinksValidImage: BaseComponent = new BaseComponent(
    'div:nth-child(2) > img:nth-child(2)'
  );
  brokenLinksBrokenImage: BaseComponent = new BaseComponent(
    'div:nth-child(2) > img:nth-child(6)'
  );
  brokenLinksValidLink: BaseComponent = new BaseComponent(
    'div:nth-child(2) > a:nth-child(10)'
  );
  brokenLinksBrokenLink: BaseComponent = new BaseComponent(
    'div:nth-child(2) > a:nth-child(14)'
  );

  openBrokenLinksCategory() {
    this.brokenLinksCategory.clickElement();
  }

  validateCategoryLinksAndImages() {
    this.brokenLinksValidImage.waitForVisible();
    this.brokenLinksBrokenImage.waitForVisible();
    this.brokenLinksValidLink.clickElement();
    I.seeCurrentUrlEquals(this.currentUrlValidLink);
    I.executeScript('window.history.back();');
    this.brokenLinksBrokenLink.clickElement();
    I.seeCurrentUrlEquals(this.currentUrlInvalidLink);
  }
}

export = new BrokenLinksPage();
