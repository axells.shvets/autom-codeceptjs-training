import { BaseComponent } from '../components/baseComponent';

const { I } = inject();
const eachElement = codeceptjs.container.plugins('eachElement');

class LandingPage {
  headLogo: BaseComponent = new BaseComponent('header  img[src*=Toolsqa]');
  elementsNavBar: BaseComponent = new BaseComponent(
    '//h5[contains(text(),"Elements")]'
  );
  formsNavBar: BaseComponent = new BaseComponent(
    '//h5[contains(text(),"Forms")]'
  );
  alertsNavBar: BaseComponent = new BaseComponent(
    '//h5[contains(text(),"Alerts")]'
  );
  widgetsNavBar: BaseComponent = new BaseComponent(
    '//h5[contains(text(),"Widgets")]'
  );
  interactionsNavBar: BaseComponent = new BaseComponent(
    '//h5[contains(text(),"Interactions")]'
  );
  detailsNavBarAllItems: string = 'div > span.group-header';

  openElements() {
    this.elementsNavBar.clickElement();
    I.waitForVisible(this.detailsNavBarAllItems);
  }

  openForms() {
    this.formsNavBar.clickElement();
    I.waitForVisible(this.detailsNavBarAllItems);
  }

  openAlerts() {
    this.alertsNavBar.clickElement();
    I.waitForVisible(this.detailsNavBarAllItems);
  }

  openWidgets() {
    this.widgetsNavBar.clickElement();
    I.waitForVisible(this.detailsNavBarAllItems);
  }

  openInteractions() {
    this.interactionsNavBar.clickElement();
    I.waitForVisible(this.detailsNavBarAllItems);
  }

  async goOverAllCategories() {
    await eachElement(
      'click all nav menu items',
      this.detailsNavBarAllItems,
      async (el, index) => {
        if (index < 5) await el.click();
      }
    );
  }
}

export = new LandingPage();
