import { BaseComponent } from '../../components/baseComponent';
import { SliderComponent } from '../../components/sliderComponent';

const { I } = inject();

class SliderPage {
  sliderCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Slider")]'
  );
  sliderSlider: SliderComponent = new SliderComponent('input[type="range"]');

  openSliderCategory() {
    this.sliderCategory.clickElement();
  }

  changeSliderPosition() {
    this.sliderSlider.dragSliderToPosition(300);
  }
}

export = new SliderPage();
