import { BaseComponent } from './baseComponent';

const { I } = inject();

export class SliderComponent extends BaseComponent {
  constructor(selector: string) {
    super(selector);
  }

  dragSliderToPosition(position: number) {
    I.waitForVisible(this.getSelector());
    I.dragSlider(this.getSelector(), position);
  }

  async getAllObjectsAsArray(): Promise<SliderComponent[]> {
    return await this.getAllObjectsAsSameType<SliderComponent>(SliderComponent);
  }
}
