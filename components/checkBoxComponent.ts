import { BaseComponent } from './baseComponent';

const { I } = inject();

export class CheckBox extends BaseComponent {
  constructor(selector: string) {
    super(selector);
  }

  setCheckBoxTrue() {
    I.waitForElement(this.getSelector());
    I.checkOption(this.getSelector());
  }

  setCheckBoxFalse() {
    I.waitForElement(this.getSelector());
    I.uncheckOption(this.getSelector());
  }

  seeCheckBoxSetTrue() {
    I.waitForVisible(this.getSelector());
    I.seeCheckboxIsChecked(this.getSelector());
  }

  seeCheckBoxSetFalse() {
    I.waitForVisible(this.getSelector());
    I.dontSeeCheckboxIsChecked(this.getSelector());
  }

  async getAllCheckBoxObjects(
    replaceIndex: boolean = false
  ): Promise<CheckBox[]> {
    return await this.getAllObjectsAsSameType<CheckBox>(CheckBox, replaceIndex);
  }

  async getAllObjectsAsArray(): Promise<CheckBox[]> {
    return await this.getAllObjectsAsSameType<CheckBox>(CheckBox);
  }
}
