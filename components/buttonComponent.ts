import { BaseComponent } from './baseComponent';

const { I } = inject();

export class Button extends BaseComponent {
  constructor(selector: string) {
    super(selector);
  }

  clickButton() {
    I.waitForElement(this.getSelector());
    I.click(this.getSelector());
  }

  doubleClickButton() {
    I.waitForElement(this.getSelector());
    I.doubleClick(this.getSelector());
  }

  rightClickButton() {
    I.waitForElement(this.getSelector());
    I.rightClick(this.getSelector());
  }

  async getAllObjectsAsArray(): Promise<Button[]> {
    return await this.getAllObjectsAsSameType<Button>(Button);
  }
}
