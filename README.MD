!!!Installation!!!
Installing apps:
npm install
npx playwright install 

!!!Run tests!!!
npx codeceptjs run
From specific file:
npx codeceptjs run <filepath>
With specific tag from file:
npx codeceptjs run <filepath(optional)> --grep <'@tag_here'>
With reporter (reports are stored in output/reports):
npx codeceptjs run --reporter mochawesome
