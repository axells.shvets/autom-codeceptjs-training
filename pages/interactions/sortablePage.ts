import assert from 'assert';
import { BaseComponent } from '../../components/baseComponent';
import { DraggableComponent } from '../../components/draggableComponent';

const { I } = inject();

class SortablePage {
  protected sortableDragTarget: string = '#demo-tab-list';

  sortableCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Sortable")]'
  );
  sortableListTab: BaseComponent = new BaseComponent('#demo-tab-list');
  sortableGridTab: BaseComponent = new BaseComponent('#demo-tab-grid');
  sortableVerticalList: DraggableComponent = new DraggableComponent(
    '//div[@class="vertical-list-container mt-4"]/div'
  );
  sortableGridList: DraggableComponent = new DraggableComponent(
    '//div[@class="grid-container mt-4"]//div[starts-with(@class,"list")]'
  );

  openSortableCategory() {
    this.sortableCategory.clickElement();
  }

  async validateVerticalSortableList() {
    this.sortableListTab.clickElement();
    const dragableVerticalListItems =
      await this.sortableVerticalList.getAllObjectsAsArray();
    let originalValue = await dragableVerticalListItems[0].getTextValue();
    for (let i = 0; i < dragableVerticalListItems.length; i++) {
      dragableVerticalListItems[i].dragAndDropElement(this.sortableDragTarget);
      let dragableVerticalSortedList =
        await this.sortableVerticalList.getAllObjectsAsArray();
      let sortedValue = await dragableVerticalSortedList[0].getTextValue();
      if (i > 0) {
        assert(sortedValue !== originalValue);
      }
    }
  }

  async validateGridSortableList() {
    this.sortableGridTab.clickElement();
    const dragableGridListItems =
      await this.sortableGridList.getAllObjectsAsArray();
    let originalValue = await dragableGridListItems[0].getTextValue();
    for (let i = 0; i < dragableGridListItems.length; i++) {
      dragableGridListItems[i].dragAndDropElement(this.sortableDragTarget);
      let dragableGridSortedList =
        await this.sortableGridList.getAllObjectsAsArray();
      let sortedValue = await dragableGridSortedList[0].getTextValue();
      if (i > 0) {
        assert(sortedValue !== originalValue);
      }
    }
  }
}

export = new SortablePage();
