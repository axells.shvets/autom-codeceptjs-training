import { BaseComponent } from '../../components/baseComponent';
import { CheckBox } from '../../components/checkBoxComponent';
import { EditBox } from '../../components/editBoxComponent';
import { DropDown } from '../../components/dropDownComponent';
import {
  dataSingle,
  dataSingleDateAndTime,
} from '../../data/dataProvider/testDataProviderTypes';
import { SelectUploadFile } from '../../components/selectUploadFileComponent';
import { Button } from '../../components/buttonComponent';

const { I } = inject();

class PracticeFormPage {
  protected uploadFilePath: string = './data/testData.js';

  practiceFormCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Practice Form")]'
  );
  practiceFormFirstNameField: EditBox = new EditBox('#firstName');
  practiceFormLastNameField: EditBox = new EditBox('#lastName');
  practiceFormEmailField: EditBox = new EditBox('#userEmail');
  practiceFormGenderMaleRadioButton: CheckBox = new CheckBox('#gender-radio-1');
  practiceFormGenderFemaleRadioButton: CheckBox = new CheckBox(
    '#gender-radio-2'
  );
  practiceFormGenderOtherRadioButton: CheckBox = new CheckBox(
    '#gender-radio-3'
  );
  practiceFormMobileField: EditBox = new EditBox('#userNumber');
  practiceFormDateOfBirthDatePicker: BaseComponent = new BaseComponent(
    '#dateOfBirthInput'
  );
  practiceFormDateOfBirthMonthDropDown: DropDown = new DropDown(
    '.react-datepicker__month-select'
  );
  practiceFormDateOfBirthYearsDropDown: DropDown = new DropDown(
    '.react-datepicker__year-select'
  );
  practiceFormSubjectsAutocompleteField: EditBox = new EditBox(
    '#subjectsInput'
  );
  practiceFormHobbiesSportsCheckbox: CheckBox = new CheckBox(
    '#hobbies-checkbox-1'
  );
  practiceFormHobbiesReadingCheckBox: CheckBox = new CheckBox(
    '#hobbies-checkbox-2'
  );
  practiceFormHobbiesMusicCheckbox: CheckBox = new CheckBox(
    '#hobbies-checkbox-3'
  );
  practiceFormPictureSelectFile: SelectUploadFile = new SelectUploadFile(
    '#uploadPicture'
  );
  practiceFormCurrentAddressField: EditBox = new EditBox('#currentAddress');
  practiceFormStateAndCityStateDropDown: EditBox = new EditBox(
    '#react-select-3-input'
  );
  practiceFormStateAndCityCityDropDown: EditBox = new EditBox(
    '#react-select-4-input'
  );
  practiceFormSubmitButton: Button = new Button('#submit');
  practiceFormMonthDates: BaseComponent = new BaseComponent(
    '.react-datepicker__day--001'
  );
  practiceFormSubmitModalTitle: BaseComponent = new BaseComponent(
    '#example-modal-sizes-title-lg'
  );
  practiceFormSubmitModalStudentNameText: BaseComponent = new BaseComponent(
    '//tr[1]/td[2]'
  );
  practiceFormSubmitModalStudentEmailText: BaseComponent = new BaseComponent(
    '//tr[2]/td[2]'
  );
  practiceFormSubmitModalStudentMobileText: BaseComponent = new BaseComponent(
    '//tr[4]/td[2]'
  );
  practiceFormSubmitModalStudentDoBText: BaseComponent = new BaseComponent(
    '//tr[5]/td[2]'
  );
  practiceFormSubmitModalSubjectsText: BaseComponent = new BaseComponent(
    '//tr[6]/td[2]'
  );
  practiceFormSubmitModalAddressText: BaseComponent = new BaseComponent(
    '//tr[9]/td[2]'
  );
  practiceFormSubmitModalStateAndCityText: BaseComponent = new BaseComponent(
    '//tr[10]/td[2]'
  );

  //!!!Temporary solution!!! Datepicker date references to implement properly with dynamic date selection
  practiceFormDatePickerDates: BaseComponent = new BaseComponent(
    '.react-datepicker__day--001'
  );

  practiceFormsAllCheckboxes: any[] = [
    this.practiceFormHobbiesSportsCheckbox,
    this.practiceFormHobbiesReadingCheckBox,
    this.practiceFormHobbiesMusicCheckbox,
  ];

  openPracticeForm() {
    this.practiceFormCategory.clickElement();
  }

  submitPracticeForm(
    testDataSingle: dataSingle,
    dataSingleDateAndTime: dataSingleDateAndTime
  ) {
    this.practiceFormFirstNameField.fillFieldWithNewText(
      testDataSingle.firstName
    );
    this.practiceFormLastNameField.fillFieldWithNewText(
      testDataSingle.lastName
    );
    this.practiceFormEmailField.fillFieldWithNewText(testDataSingle.email);
    this.practiceFormMobileField.fillFieldWithNewText(testDataSingle.mobile);
    this.practiceFormGenderFemaleRadioButton.setCheckBoxTrue();
    this.practiceFormGenderMaleRadioButton.setCheckBoxTrue();
    this.practiceFormGenderOtherRadioButton.setCheckBoxTrue();
    this.practiceFormDateOfBirthDatePicker.clickElement();

    this.practiceFormDateOfBirthMonthDropDown.selectSelectOption(
      dataSingleDateAndTime.monthsList[2]
    );

    this.practiceFormDateOfBirthYearsDropDown.selectSelectOption(
      dataSingleDateAndTime.yearsList[2]
    );
    this.practiceFormDatePickerDates.clickElement();
    for (let i = 0; i < testDataSingle.subjects.length; i++) {
      this.practiceFormSubjectsAutocompleteField.addTextToField(
        testDataSingle.subjects[i]
      );
      I.pressKey('Enter');
    }
    for (let i = 0; i < this.practiceFormsAllCheckboxes.length; i++) {
      this.practiceFormsAllCheckboxes[i].setCheckBoxTrue();
      this.practiceFormsAllCheckboxes[i].setCheckBoxFalse();
      this.practiceFormsAllCheckboxes[i].setCheckBoxTrue();
    }
    this.practiceFormPictureSelectFile.selectFileToAttach(this.uploadFilePath);

    this.practiceFormCurrentAddressField.fillFieldWithNewText(
      testDataSingle.currentAddress
    );
    this.practiceFormStateAndCityStateDropDown.fillFieldWithNewText(
      testDataSingle.state
    );
    I.pressKey('Enter');
    this.practiceFormStateAndCityCityDropDown.fillFieldWithNewText(
      testDataSingle.city
    );
    I.pressKey('Enter');
    this.practiceFormSubmitButton.clickButton();
    this.practiceFormSubmitModalTitle.waitForVisible();
    this.practiceFormSubmitModalStudentNameText.waitForTextOnElement(
      `${testDataSingle.firstName + ' ' + testDataSingle.lastName}`
    );
    this.practiceFormSubmitModalStudentEmailText.waitForTextOnElement(
      `${testDataSingle.email}`
    );
    this.practiceFormSubmitModalStudentMobileText.waitForTextOnElement(
      `${testDataSingle.mobile}`
    );
    this.practiceFormSubmitModalStudentDoBText.waitForTextOnElement(
      `${
        '01 ' +
        dataSingleDateAndTime.monthsList[2] +
        ',' +
        dataSingleDateAndTime.yearsList[2]
      }`
    );

    //correct this embarasment later
    // I.seeTextEquals(
    //   `${subjects.join(', ')}`,
    //   this.practiceFormSubmitModalSubjectsText
    // );

    this.practiceFormSubmitModalAddressText.waitForTextOnElement(
      `${testDataSingle.currentAddress}`
    );
    this.practiceFormSubmitModalStateAndCityText.waitForTextOnElement(
      `${testDataSingle.state + ' ' + testDataSingle.city}`
    );
  }
}

export = new PracticeFormPage();
