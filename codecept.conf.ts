import { setHeadlessWhen, setCommonPlugins } from '@codeceptjs/configure';
// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

// enable all common plugins https://github.com/codeceptjs/configure#setcommonplugins
setCommonPlugins();

export const config: CodeceptJS.MainConfig = {
  tests: './tests/*Test.ts',
  output: './output',
  helpers: {
    Playwright: {
      url: 'https://demoqa.com',
      show: true,
      browser: 'chromium',
      windowSize: '1920x1080',
      waitForTimeout: 30000,
    },
    FileSystem: {},
    Mochawesome: {
      uniqueScreenshotNames: 'true',
    },
  },
  mocha: {
    reporterOptions: {
      reportDir: 'output/reports',
      reportFilename: '[status]_[datetime]-[name]-report',
      timestamp: 'longDate',
      html: true,
      json: false,
      quiet: true,
    },
  },
  include: {
    landingPage: './pages/landingPage.ts',
    elementsTextBoxPage: './pages/elementsPages/textBoxPage.ts',
    elementsCheckBoxPage: './pages/elementsPages/checkBoxPage.ts',
    elementsRadioButtonPage: './pages/elementsPages/radioButtonPage.ts',
    elementsWebTablesPage: './pages/elementsPages/webTablesPage.ts',
    elementsButtonsPage: './pages/elementsPages/buttonsPage.ts',
    elementsLinksPage: './pages/elementsPages/linksPage.ts',
    elementsBrokenLinksPage: './pages/elementsPages/brokenLinksPage.ts',
    elementsUploadsAndDownloadsPage:
      './pages/elementsPages/uploadAndDownloadPage.ts',
    elementsDynamicPropertiesPage:
      './pages/elementsPages/dynamicPropertiesPage.ts',
    formsPracticeFormPage: './pages/formsPages/practiceFormPage.ts',
    alertsFramesAndWindowsBrowserWindowsPage:
      './pages/alertsFramesAndWindowsPages/browserWindowsPage.ts',
    alertsFramesAndWindowsAlertsPage:
      './pages/alertsFramesAndWindowsPages/alertsPage.ts',
    alertsFramesAndWindowsFramesPage:
      './pages/alertsFramesAndWindowsPages/framesPage.ts',
    alertsFramesAndWindowsNestedFramesPage:
      './pages/alertsFramesAndWindowsPages/nestedFramesPage.ts',
    alertsFramesAndWindowsModalDialogsPage:
      './pages/alertsFramesAndWindowsPages/modalDialogsPage.ts',
    widgetsAccordianPage: './pages/widgets/accordianPage.ts',
    widgetsAutoCompletePage: './pages/widgets/autoCompletePage.ts',
    widgetsDatePickerPage: './pages/widgets/datePickerPage.ts',
    widgetsSliderPage: './pages/widgets/sliderPage.ts',
    widgetsProgressBarPage: './pages/widgets/progressBarPage.ts',
    widgetsTabsPage: './pages/widgets/tabsPage.ts',
    widgetsToolTipsPage: './pages/widgets/toolTipsPage.ts',
    widgetsMenuPage: './pages/widgets/menuPage.ts',
    widgetsSelectMenuPage: './pages/widgets/selectMenuPage.ts',
    interactionsSortablePage: './pages/interactions/sortablePage.ts',
    interactionsSelectablePage: './pages/interactions/selectablePage.ts',
    interactionsResizablePage: './pages/interactions/resizablePage.ts',
    interactionsDroppablePage: './pages/interactions/droppablePage.ts',
    interactionsDraggablePage: './pages/interactions/draggablePage.ts',
  },
  name: 'codeceptjs',
};
