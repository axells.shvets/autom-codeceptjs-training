import {
  testDataSingle,
  testDataForFieldValidation,
  colors,
  testDataDate,
} from '../data/testDataSingle';
import landingPage from '../pages/landingPage';

const testData = require('../data/testData')();

Feature('Training - E2E Tests');

Before(({ I }) => {
  I.amOnPage('');
  landingPage.headLogo.waitForVisible();
});

Scenario('Navigation menu validation', ({ landingPage }) => {
  landingPage.openElements();
  landingPage.goOverAllCategories();
});

Scenario(
  'Elements: Text Box Category Validation',
  ({ landingPage, elementsTextBoxPage }) => {
    landingPage.openElements();
    elementsTextBoxPage.submitTextBoxIncorrectEmail(testDataSingle);
    elementsTextBoxPage.submitTextBoxForm(testDataSingle);
  }
);

Scenario(
  'Elements: Check Box Category Validation',
  ({ landingPage, elementsCheckBoxPage }) => {
    landingPage.openElements();
    elementsCheckBoxPage.openCheckBoxCategory();
    elementsCheckBoxPage.expandAllCheckBoxListItems();
    elementsCheckBoxPage.validateCheckBoxesStatusUnchecked();
    elementsCheckBoxPage.checkHomeCheckBox();
    elementsCheckBoxPage.validateCheckBoxesStatusChecked();
    elementsCheckBoxPage.checkHomeCheckBox();
    elementsCheckBoxPage.validateCheckBoxesStatusUnchecked();
    elementsCheckBoxPage.collapseAllCheckBoxListItems();
    elementsCheckBoxPage.validateCheckBoxesStatusUnchecked();
  }
);

Scenario(
  'Elements: Radio Button Category Validation',
  ({ landingPage, elementsRadioButtonPage }) => {
    landingPage.openElements();
    elementsRadioButtonPage.openRadioButtonCategory();
    elementsRadioButtonPage.setTrueYesRadioButton();
    elementsRadioButtonPage.setTrueImpressiveRadioButton();
    elementsRadioButtonPage.seeNoRadioButtonDisabled();
  }
);

Scenario(
  'Elements: Web tables Category Validation',
  async ({ landingPage, elementsWebTablesPage }) => {
    landingPage.openElements();
    elementsWebTablesPage.openWebTablesCategory();
    elementsWebTablesPage.openAddRecordModal();
    await elementsWebTablesPage.submitRegistrationForm(testDataSingle);
    await elementsWebTablesPage.editTableRecord(testDataSingle);
    elementsWebTablesPage.openAddRecordModal();
    for (let i = 0; i < testDataForFieldValidation.length; i++) {
      await elementsWebTablesPage.validateRegistrationModalFields(
        testDataForFieldValidation[i]
      );
    }
    elementsWebTablesPage.closeAddRecordModal();
    await elementsWebTablesPage.deleteTableRow();
  }
);

Scenario(
  'Elements: Buttons Category Validation',
  ({ landingPage, elementsButtonsPage }) => {
    landingPage.openElements();
    elementsButtonsPage.openButtonsCategory();
    elementsButtonsPage.clickAllButtonsPageButtons();
  }
);

Scenario(
  'Elements: Links Category Validation',
  async ({ landingPage, elementsLinksPage }) => {
    landingPage.openElements();
    await elementsLinksPage.openLinksCategory();
    await elementsLinksPage.clickAndValidateAllPageLinks();
  }
);

Scenario(
  'Elements: Broken Links Category Validation',
  ({ landingPage, elementsBrokenLinksPage }) => {
    landingPage.openElements();
    elementsBrokenLinksPage.openBrokenLinksCategory();
    elementsBrokenLinksPage.validateCategoryLinksAndImages();
  }
);

Scenario(
  'Elements: Uploads and Downloads Category Validation',
  ({ landingPage, elementsUploadsAndDownloadsPage }) => {
    landingPage.openElements();
    elementsUploadsAndDownloadsPage.openUploadAndDownloadCategory();
    elementsUploadsAndDownloadsPage.downloadSampleFile();
    elementsUploadsAndDownloadsPage.uploadSampleFile();
  }
);

Scenario(
  'Elements: Dynamic Properties Category Validation',
  ({ landingPage, elementsDynamicPropertiesPage }) => {
    landingPage.openElements();
    elementsDynamicPropertiesPage.openDynamicProperties();
    elementsDynamicPropertiesPage.validatePageDynamicElements();
  }
);

Scenario(
  'Forms: Practice Form Category Validation',
  ({ landingPage, formsPracticeFormPage }) => {
    landingPage.openForms();
    formsPracticeFormPage.openPracticeForm();
    formsPracticeFormPage.submitPracticeForm(testDataSingle, testDataDate);
  }
);

Scenario(
  'Alerts, Frame & Windows: Browser Windows Category Validation',
  ({ landingPage, alertsFramesAndWindowsBrowserWindowsPage }) => {
    landingPage.openAlerts();
    alertsFramesAndWindowsBrowserWindowsPage.openBrowserWindowsCategory();
    alertsFramesAndWindowsBrowserWindowsPage.validateBrowserWindowsPageButtons();
  }
);

Scenario(
  'Alerts, Frame & Windows: Alerts Category Validation',
  ({ landingPage, alertsFramesAndWindowsAlertsPage }) => {
    landingPage.openAlerts();
    alertsFramesAndWindowsAlertsPage.openAlertsCategory();
    alertsFramesAndWindowsAlertsPage.acceptAllPageAlerts();
  }
);

// Scenario(
//   'Alerts, Frame & Windows: Frames Category Validation',
//   ({ landingPage, alertsFramesAndWindowsFramesPage }) => {
//     landingPage.openAlerts();
//     alertsFramesAndWindowsFramesPage.openFramesCategory();
//     alertsFramesAndWindowsFramesPage.switchBetweenIframes();
//   }
// );

// Scenario(
//   'Alerts, Frame & Windows: Nested Frames Category Validation',
//   ({ landingPage, alertsFramesAndWindowsNestedFramesPage }) => {
//     landingPage.openAlerts();
//     alertsFramesAndWindowsNestedFramesPage.openNestedFramesCategory();
//     alertsFramesAndWindowsNestedFramesPage.switchBetweenNestedIframes();
//   }
// );

Scenario(
  'Alerts, Frame & Windows: Modal Dialogs Category Validation',
  ({ landingPage, alertsFramesAndWindowsModalDialogsPage }) => {
    landingPage.openAlerts();
    alertsFramesAndWindowsModalDialogsPage.openModalDialogsCategory();
    alertsFramesAndWindowsModalDialogsPage.openAndCloseAllModals();
    alertsFramesAndWindowsModalDialogsPage.openAndCloseAllModalsViaIcon();
  }
);

Scenario(
  'Widgets: Accordian Category Validation',
  ({ landingPage, widgetsAccordianPage }) => {
    landingPage.openWidgets();
    widgetsAccordianPage.openAccordianCategory();
    widgetsAccordianPage.goOverAccordianSections();
  }
);

Scenario(
  'Widgets: Auto Complete Category Validation',
  async ({ landingPage, widgetsAutoCompletePage }) => {
    landingPage.openWidgets();
    widgetsAutoCompletePage.openAutoCompleteCategory();
    await widgetsAutoCompletePage.fillInAndValidateAutocompleteMultipleField(
      colors
    );
    await widgetsAutoCompletePage.fillInAndValidateAutocompleteSingle(colors);
  }
);

Scenario(
  'Widgets: Date Picker Category Validation',
  ({ landingPage, widgetsDatePickerPage }) => {
    landingPage.openWidgets();
    widgetsDatePickerPage.openDatePickerCategory();
    widgetsDatePickerPage.selectDateAllDates(testDataDate);
    widgetsDatePickerPage.selectDateAndTimeAllDates(testDataDate);
  }
);

Scenario(
  'Widgets: Slider Category Validation',
  ({ landingPage, widgetsSliderPage }) => {
    landingPage.openWidgets();
    widgetsSliderPage.openSliderCategory();
    widgetsSliderPage.changeSliderPosition();
  }
);

Scenario(
  'Widgets: Progress Bar Category Validation',
  async ({ landingPage, widgetsProgressBarPage }) => {
    landingPage.openWidgets();
    widgetsProgressBarPage.openProgressBarCategory();
    await widgetsProgressBarPage.startAndResetProgressBar();
  }
);

Scenario(
  'Widgets: Tabs Category Validation',
  async ({ landingPage, widgetsTabsPage }) => {
    landingPage.openWidgets();
    widgetsTabsPage.openTabsCategory();
    await widgetsTabsPage.goOverAllTabs();
  }
);

// Scenario(
//   'Widgets: Tool Tips Category Validation',
//   async ({ landingPage, widgetsToolTipsPage }) => {
//     landingPage.openWidgets();
//     widgetsToolTipsPage.openToolTipsCategory();
//     await widgetsToolTipsPage.hoverAndCheckToolTips();
//   }
// );

Scenario(
  'Widgets: Menu Category Validation',
  async ({ landingPage, widgetsMenuPage }) => {
    landingPage.openWidgets();
    widgetsMenuPage.openMenuCategory();
    await widgetsMenuPage.hoverAllMenuItems();
  }
);

Scenario(
  'Widgets: Select Menu Category Validation @current',
  async ({ landingPage, widgetsSelectMenuPage }) => {
    landingPage.openWidgets();
    widgetsSelectMenuPage.openSelectMenuCategory();
    await widgetsSelectMenuPage.validateAllSelectVariants();
  }
);

// Scenario(
//   'Interactions: Sortable Category Validation',
//   async ({ landingPage, interactionsSortablePage }) => {
//     landingPage.openInteractions();
//     interactionsSortablePage.openSortableCategory();
//     await interactionsSortablePage.validateVerticalSortableList();
//     await interactionsSortablePage.validateGridSortableList();
//   }
// );

Scenario(
  'Interactions: Selectable Category Validation',
  async ({ landingPage, interactionsSelectablePage }) => {
    landingPage.openInteractions();
    interactionsSelectablePage.openSelectableCategory();
    await interactionsSelectablePage.clickAllListItems();
    await interactionsSelectablePage.checkIfListItemSelected();
    await interactionsSelectablePage.clickAllGridItems();
    await interactionsSelectablePage.checkIfGridItemSelected();
    await interactionsSelectablePage.clickAllListItems();
    await interactionsSelectablePage.checkIfListItemUnSelected();
    await interactionsSelectablePage.clickAllGridItems();
    await interactionsSelectablePage.checkIfGridItemUnSelected();
  }
);

// Scenario(
//   'Interactions: Resizable Category Validation',
//   async ({ landingPage, interactionsResizablePage }) => {
//     landingPage.openInteractions();
//     interactionsResizablePage.openResizableCategory();
//     await interactionsResizablePage.resizeRestrictedResizableBox();
//     await interactionsResizablePage.resizeFreeResizableBox();
//   }
// );

// Scenario(
//   'Interactions: Droppable Category Validation',
//   async ({ landingPage, interactionsDroppablePage }) => {
//     landingPage.openInteractions();
//     interactionsDroppablePage.openDroppableCategory();
//     interactionsDroppablePage.dragAndDropSimpleTab();
//     await interactionsDroppablePage.dragAndDropAcceptTab();
//     await interactionsDroppablePage.dragAndDropPreventPropagationTab();
//     await interactionsDroppablePage.dragAndDropRevertDraggableTab();
//   }
// );

// Scenario(
//   'Interactions: Draggable Category Validation',
//   async ({ landingPage, interactionsDraggablePage }) => {
//     landingPage.openInteractions();
//     interactionsDraggablePage.openDraggableCategory();
//     interactionsDraggablePage.simpleCategoryValidation();
//     interactionsDraggablePage.axisRestrictedValidation();
//     interactionsDraggablePage.containerRestrictedValidation();
//     interactionsDraggablePage.cursorStileValidation();
//   }
// );

Data(testData.webTablesData).Scenario(
  'Elements: Web tables Category - Data Driven Test Demo',
  async ({ landingPage, elementsWebTablesPage, current }) => {
    landingPage.openElements();
    elementsWebTablesPage.openWebTablesCategory();
    elementsWebTablesPage.openAddRecordModal();
    await elementsWebTablesPage.submitRegistrationFormBDD(
      current.firstName,
      current.lastName,
      current.email,
      current.age,
      current.salary,
      current.department
    );
  }
);
