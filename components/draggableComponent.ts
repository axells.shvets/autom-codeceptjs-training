import { BaseComponent } from './baseComponent';

const { I } = inject();

export class DraggableComponent extends BaseComponent {
  protected container: string;
  protected position: any;

  constructor(selector: string, container?: string, position?: any) {
    super(selector);
    this.container = container;
    this.position = position;
  }

  dragAndDropElement(container?: string) {
    I.dragAndDrop(this.getSelector(), this.container || container);
  }

  // specify coordinates for source position I.dragAndDrop('img.src', 'img.dst', { sourcePosition: {x: 10, y: 10} })>
  //By default option force: true is set
  dragAndDropElementWithSpecificHandlePosition(
    container: string,
    position: any
  ) {
    I.dragAndDrop(this.getSelector(), container, position);
  }

  async getAllObjectsAsArray(): Promise<DraggableComponent[]> {
    return await this.getAllObjectsAsSameType<DraggableComponent>(
      DraggableComponent
    );
  }
}
