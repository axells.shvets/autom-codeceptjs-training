import assert from 'assert';
import { BaseComponent } from '../../components/baseComponent';
import { Button } from '../../components/buttonComponent';

const { I } = inject();
const eachElement = codeceptjs.container.plugins('eachElement');

class CheckBoxPage {
  protected NUMBER_OF_CHECKBOXES_ORIGIN: number = 1;
  protected NUMBER_OF_CHECKBOXES_TOTAL: number = 17;
  checkBoxCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Check Box")]'
  );
  checkBoxExpandAllIcon: Button = new Button('.rct-option-expand-all');
  checkBoxCollapseAllIcon: Button = new Button('.rct-option-collapse-all');
  checkBoxExpandedCount: string = 'span.rct-checkbox';
  checkBoxHomeCheckBox: Button = new Button('#tree-node-home');

  openCheckBoxCategory() {
    this.checkBoxCategory.clickElement();
  }

  expandAllCheckBoxListItems() {
    I.seeNumberOfElements(
      this.checkBoxExpandedCount,
      this.NUMBER_OF_CHECKBOXES_ORIGIN
    );
    this.checkBoxExpandAllIcon.clickButton();
    I.seeNumberOfElements(
      this.checkBoxExpandedCount,
      this.NUMBER_OF_CHECKBOXES_TOTAL
    );
  }

  collapseAllCheckBoxListItems() {
    this.checkBoxCollapseAllIcon.clickButton();
    I.seeNumberOfElements(
      this.checkBoxExpandedCount,
      this.NUMBER_OF_CHECKBOXES_ORIGIN
    );
  }

  //validate default checkbox status is Disabled
  async validateCheckBoxesStatusUnchecked() {
    await eachElement(
      'validate checkbox status',
      this.checkBoxExpandedCount,
      async (el) => {
        assert(!(await el.isChecked()));
        assert(await el.isVisible());
        assert(await el.isEditable());
      }
    );
  }

  async validateCheckBoxesStatusChecked() {
    await eachElement(
      'validate checkbox status',
      this.checkBoxExpandedCount,
      async (el) => {
        assert(await el.isChecked());
        assert(await el.isVisible());
        assert(await el.isEditable());
      }
    );
  }

  checkHomeCheckBox() {
    this.checkBoxHomeCheckBox.clickByJS();
  }
}

export = new CheckBoxPage();
