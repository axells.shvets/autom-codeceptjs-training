import { BaseComponent } from '../../components/baseComponent';
import { FrameComponent } from '../../components/frameComponent';

const { I } = inject();

class NestedFramesPage {
  nestedFramesCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Nested Frames")]'
  );
  nestedFramesFirstIframe: FrameComponent = new FrameComponent('#frame1');
  nestedFramesSecondIframe: FrameComponent = new FrameComponent(
    'body > iframe'
  );
  nestedFramesFirstIframeText: BaseComponent = new BaseComponent(
    '"Parent frame"'
  );
  nestedFramesSecondIframeText: BaseComponent = new BaseComponent('body > p');

  openNestedFramesCategory() {
    this.nestedFramesCategory.clickElement();
  }

  switchBetweenNestedIframes() {
    this.nestedFramesFirstIframe.switchBetweenFrames();
    this.nestedFramesSecondIframe.waitForVisible();
    this.nestedFramesCategory.waitToHide();
    this.nestedFramesSecondIframe.switchBetweenFrames();
    this.nestedFramesSecondIframeText.waitForVisible();
    this.nestedFramesFirstIframe.waitToHide();
    I.switchTo();
    this.nestedFramesCategory.waitForVisible();
  }
}

export = new NestedFramesPage();
