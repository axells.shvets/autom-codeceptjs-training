import { BaseComponent } from '../../components/baseComponent';
import { Button } from '../../components/buttonComponent';
import { EditBox } from '../../components/editBoxComponent';
import { dataSingle } from '../../data/dataProvider/testDataProviderTypes';

class TextBoxPage {
  protected INVALID_FIELD_INPUT_ATTRIBUTE: object = {
    class: 'mr-sm-2 field-error form-control',
  };

  textBoxCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Text Box")]'
  );
  textBoxFullNameField: EditBox = new EditBox('#userName');
  textBoxEmailField: EditBox = new EditBox('#userEmail');
  textBoxCurrentAddressField: EditBox = new EditBox('#currentAddress');
  textBoxPermanentAddressField: EditBox = new EditBox('#permanentAddress');
  textBoxSubmitButton: Button = new Button('#submit');
  textBoxSuccessName: BaseComponent = new BaseComponent('div > p#name');
  textBoxSuccessEmail: BaseComponent = new BaseComponent('div > p#email');
  textBoxSuccessCurrentAddress: BaseComponent = new BaseComponent(
    'div > p#currentAddress'
  );
  textBoxSuccessPermanentAddress: BaseComponent = new BaseComponent(
    'div > p#permanentAddress'
  );

  submitTextBoxForm(testDataSingle: dataSingle) {
    this.textBoxCategory.clickElement();

    this.textBoxFullNameField.fillFieldWithNewText(
      testDataSingle.firstName + testDataSingle.lastName
    );
    this.textBoxEmailField.fillFieldWithNewText(testDataSingle.email);
    this.textBoxCurrentAddressField.fillFieldWithNewText(
      testDataSingle.currentAddress
    );

    this.textBoxPermanentAddressField.fillFieldWithNewText(
      testDataSingle.permanentAddress
    );
    this.textBoxSubmitButton.clickButton();
    this.textBoxSuccessName.waitForTextOnElement(
      testDataSingle.firstName + testDataSingle.lastName
    );
    this.textBoxSuccessEmail.waitForTextOnElement(testDataSingle.email);
    this.textBoxSuccessCurrentAddress.waitForTextOnElement(
      testDataSingle.currentAddress
    );
    this.textBoxSuccessPermanentAddress.waitForTextOnElement(
      testDataSingle.permanentAddress
    );
  }

  submitTextBoxIncorrectEmail(testDataSingle: dataSingle) {
    this.textBoxCategory.clickElement();

    this.textBoxEmailField.fillFieldWithNewText(
      testDataSingle.email + testDataSingle.mobile
    );
    this.textBoxSubmitButton.clickButton();

    this.textBoxEmailField.checkElementAttribute(
      this.INVALID_FIELD_INPUT_ATTRIBUTE
    );
  }
}

export = new TextBoxPage();
