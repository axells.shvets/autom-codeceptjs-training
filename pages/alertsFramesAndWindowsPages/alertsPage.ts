import { BaseComponent } from '../../components/baseComponent';
import { Button } from '../../components/buttonComponent';

const { I } = inject();

class AlertsPage {
  protected alertTimeout: number = 6;

  alertsCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Alerts")]'
  );
  alertsGenericAlertButton: Button = new Button('#alertButton');
  alertsDelayedAlertButton: Button = new Button('#timerAlertButton');
  alertsConfirmAlertButton: Button = new Button('#confirmButton');
  alertsPromptAlertButton: Button = new Button('#promtButton');
  alertsConfirmAlertMessage: BaseComponent = new BaseComponent(
    '#confirmResult'
  );
  alertsPromptAlertMessage: BaseComponent = new BaseComponent('#promptResult');
  alertsAllAlertsDetails: any[] = [
    [this.alertsGenericAlertButton, 'You clicked a button'],
    [this.alertsDelayedAlertButton, 'This alert appeared after 5 seconds'],
    [
      this.alertsConfirmAlertButton,
      'Do you confirm action?',
      this.alertsConfirmAlertMessage,
      'You selected Ok',
    ],
    [
      this.alertsPromptAlertButton,
      'Please enter your name',
      this.alertsPromptAlertMessage,
    ],
  ];

  openAlertsCategory() {
    this.alertsCategory.clickElement();
  }

  acceptAllPageAlerts() {
    for (let i = 0; i < this.alertsAllAlertsDetails.length; i++) {
      this.alertsAllAlertsDetails[i][0].clickElement();
      if (i === 1) {
        I.wait(this.alertTimeout);
      }
      I.seeInPopup(this.alertsAllAlertsDetails[i][1]);
      I.acceptPopup();
      if (i === 2) {
        this.alertsAllAlertsDetails[i][2].waitForVisible();

        this.alertsAllAlertsDetails[i][2].waitForTextOnElement(
          this.alertsAllAlertsDetails[i][3]
        );
      }
    }
  }

  //Add method to enter text into js alert pop-up...
}

export = new AlertsPage();
