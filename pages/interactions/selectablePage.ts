import assert from 'assert';
import { BaseComponent } from '../../components/baseComponent';

const { I } = inject();

class SelectablePage {
  protected activeAttributeList: Object = {
    class: 'mt-2 list-group-item active list-group-item-action',
  };
  protected activeAttributeGrid: Object = {
    class: 'list-group-item active list-group-item-action',
  };
  protected InactiveAttributeList: Object = {
    class: 'mt-2 list-group-item list-group-item-action',
  };
  protected InactiveAttributeGrid: Object = {
    class: 'list-group-item list-group-item-action',
  };

  selectableCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Selectable")]'
  );

  selectableListTab: BaseComponent = new BaseComponent('#demo-tab-list');
  selectableAllListItems: BaseComponent = new BaseComponent(
    '//ul[@id="verticalListContainer"]/li'
  );
  selectableGridTab: BaseComponent = new BaseComponent('#demo-tab-grid');
  selectableAllGridItems: BaseComponent = new BaseComponent(
    '//div[@id="gridContainer"]/descendant::li'
  );

  openSelectableCategory() {
    this.selectableCategory.clickElement();
  }

  async clickAllListItems() {
    this.selectableListTab.clickElement();
    let itemsToClick = await this.selectableAllListItems.getAllObjectsAsArray();
    for (let selectable of itemsToClick) {
      selectable.clickElement();
    }
  }

  async checkIfListItemSelected() {
    let itemsToCheck = await this.selectableAllListItems.getAllObjectsAsArray();
    for (let i = 0; i < itemsToCheck.length; i++) {
      itemsToCheck[i].checkElementAttribute(this.activeAttributeList);
    }
  }

  async clickAllGridItems() {
    this.selectableGridTab.clickElement();
    for (let selectable of await this.selectableAllGridItems.getAllObjectsAsArray()) {
      selectable.clickElement();
    }
  }

  async checkIfGridItemSelected() {
    for (let selectable of await this.selectableAllGridItems.getAllObjectsAsArray()) {
      selectable.checkElementAttribute(this.activeAttributeGrid);
    }
  }

  async checkIfListItemUnSelected() {
    for (let selectable of await this.selectableAllListItems.getAllObjectsAsArray()) {
      selectable.checkElementAttribute(this.InactiveAttributeList);
    }
  }

  async checkIfGridItemUnSelected() {
    for (let selectable of await this.selectableAllGridItems.getAllObjectsAsArray()) {
      selectable.checkElementAttribute(this.InactiveAttributeGrid);
    }
  }
}

export = new SelectablePage();
