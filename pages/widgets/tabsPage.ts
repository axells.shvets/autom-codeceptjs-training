import { BaseComponent } from '../../components/baseComponent';

const { I } = inject();

class TabsPage {
  public tabsCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Tabs")]'
  );
  public tabsAllTabs: BaseComponent = new BaseComponent(
    '#tabsContainer > nav > a'
  );
  public tabsWhatTab: BaseComponent = new BaseComponent('#demo-tab-what');
  public tabsWhatText: BaseComponent = new BaseComponent(
    '#demo-tabpane-what > p'
  );
  public tabsOriginTab: BaseComponent = new BaseComponent('#demo-tab-origin');
  public tabsOriginText: BaseComponent = new BaseComponent(
    '#demo-tabpane-origin > p'
  );
  public tabsUseTab: BaseComponent = new BaseComponent('#demo-tab-use');
  public tabsUseText: BaseComponent = new BaseComponent(
    '#demo-tabpane-use > p'
  );

  public tabsAllTabsContent = [
    [this.tabsWhatTab, this.tabsWhatText],
    [this.tabsOriginTab, this.tabsOriginText],
    [this.tabsUseTab, this.tabsUseText],
  ];

  openTabsCategory() {
    this.tabsCategory.waitForVisible();
    this.tabsCategory.clickElement();
  }

  goOverAllTabs() {
    for (const tab of this.tabsAllTabsContent) {
      tab[0].clickElement();
      tab[1].waitForVisible();
    }
  }
}

export = new TabsPage();
