import assert from 'assert';
import { BaseComponent } from '../../components/baseComponent';
import { Button } from '../../components/buttonComponent';
import { EditBox } from '../../components/editBoxComponent';

const { I } = inject();

class ToolTipsPage {
  protected toolTipsToolTipAttribute: string = 'aria-describedby';
  protected toolTipsAllToolTipText: string[] = [
    'buttonToolTip',
    'textFieldToolTip',
    'contraryTexToolTip',
    'sectionToolTip',
  ];

  protected toolTipsCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Tool Tips")]'
  );
  protected toolTipsButton: Button = new Button('#toolTipButton');
  protected toolTipsField: EditBox = new EditBox('#texFieldToolTopContainer');
  protected toolTipsLinkOne: BaseComponent = new BaseComponent(
    '#texToolTopContainer > a:nth-child(1)'
  );
  protected toolTipsLinkTwo: BaseComponent = new BaseComponent(
    '#texToolTopContainer > a:nth-child(2)'
  );
  protected tooltipsAllHoverItems = [
    this.toolTipsButton,
    this.toolTipsField,
    this.toolTipsLinkOne,
    this.toolTipsLinkTwo,
  ];

  openToolTipsCategory() {
    this.toolTipsCategory.clickElement();
  }

  async hoverAndCheckToolTips() {
    I.wait(1);
    for (let i = 0; i < this.toolTipsAllToolTipText.length; i++) {
      this.tooltipsAllHoverItems[i].hoverElement();
      let toolTip = await this.tooltipsAllHoverItems[i].grabAttribute(
        this.toolTipsToolTipAttribute
      );
      //skip for text field tool tip due to strange behavior
      if (i === 1) {
        continue;
      }
      assert(toolTip === this.toolTipsAllToolTipText[i]);
    }
  }
}

export = new ToolTipsPage();
