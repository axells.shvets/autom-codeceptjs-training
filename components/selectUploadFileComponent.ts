import { BaseComponent } from './baseComponent';

const { I } = inject();

export class SelectUploadFile extends BaseComponent {
  constructor(selector: string) {
    super(selector);
  }

  selectFileToAttach(uploadFilePath: string) {
    I.waitForVisible(this.getSelector());
    I.attachFile(this.getSelector(), uploadFilePath);
  }

  async getAllObjectsAsArray(): Promise<SelectUploadFile[]> {
    return await this.getAllObjectsAsSameType<SelectUploadFile>(
      SelectUploadFile
    );
  }
}
