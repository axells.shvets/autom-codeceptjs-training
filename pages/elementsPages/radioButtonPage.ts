import { BaseComponent } from '../../components/baseComponent';
import { CheckBox } from '../../components/checkBoxComponent';

class RadioButtonPage {
  protected DISABLED_ELEMENT_ATTRIBUTE: object = {
    class: 'custom-control-label disabled',
  };

  radioButtonCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Radio Button")]'
  );
  yesRadioButton: CheckBox = new CheckBox('label[for="yesRadio"]');
  impressioveRadioButton: CheckBox = new CheckBox(
    'label[for="impressiveRadio"]'
  );
  noRadioButton: CheckBox = new CheckBox('label[for="noRadio"]');

  openRadioButtonCategory() {
    this.radioButtonCategory.clickElement();
  }

  setTrueYesRadioButton() {
    this.yesRadioButton.setCheckBoxTrue();
    this.yesRadioButton.seeCheckBoxSetTrue();
  }

  setTrueImpressiveRadioButton() {
    this.impressioveRadioButton.setCheckBoxTrue();
    this.impressioveRadioButton.seeCheckBoxSetTrue();
  }

  seeNoRadioButtonDisabled() {
    this.noRadioButton.waitForVisible();
    this.noRadioButton.checkElementAttribute(this.DISABLED_ELEMENT_ATTRIBUTE);
  }
}

export = new RadioButtonPage();
