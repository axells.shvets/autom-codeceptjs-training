import { dataSingle } from '../../data/dataProvider/testDataProviderTypes';

const { I } = inject();

import assert from 'assert';
import { BaseComponent } from '../../components/baseComponent';
import { Button } from '../../components/buttonComponent';
import { EditBox } from '../../components/editBoxComponent';

class WebTablesPage {
  protected webTablesTableRecordsCountOnPage: string =
    '//div[@class="rt-td"][contains(text(), "@")]';

  webTablesCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Web Tables")]'
  );
  webTablesAddButton: Button = new Button('#addNewRecordButton');
  webTablesAddModalTitle: BaseComponent = new BaseComponent(
    '#registration-form-modal'
  );
  webTablesAddModalFirstNameField: EditBox = new EditBox('#firstName');
  webTablesAddModalLastNameField: EditBox = new EditBox('#lastName');
  webTablesAddModalEmailField: EditBox = new EditBox('#userEmail');
  webTablesAddModalAgeField: EditBox = new EditBox('#age');
  webTablesAddModalSalaryField: EditBox = new EditBox('#salary');
  webTablesAddModalDepartmentField: EditBox = new EditBox('#department');
  webTablesAddModalSubmitButton: Button = new Button('#submit');
  webTablesAddModalCloseButton: Button = new Button('button.close');
  webTablesTableEditIcon: Button = new Button('#edit-record-1 > svg');
  webTablesTableDeleteIcon: Button = new Button('#delete-record-1 > svg');

  openWebTablesCategory() {
    this.webTablesCategory.clickElement();
  }

  openAddRecordModal() {
    this.webTablesAddButton.clickButton();
    this.webTablesAddModalTitle.waitForVisible();
  }

  closeAddRecordModal() {
    this.webTablesAddModalCloseButton.clickButton();
    this.webTablesAddModalTitle.waitToHide();
  }

  async submitRegistrationForm(testDataSingle: dataSingle) {
    let numOfElementsInitial = await I.grabNumberOfVisibleElements(
      this.webTablesTableRecordsCountOnPage
    );
    this.webTablesAddModalFirstNameField.fillFieldWithNewText(
      testDataSingle.firstName
    );
    this.webTablesAddModalLastNameField.fillFieldWithNewText(
      testDataSingle.lastName
    );
    this.webTablesAddModalEmailField.fillFieldWithNewText(testDataSingle.email);
    this.webTablesAddModalAgeField.fillFieldWithNewText(testDataSingle.age);
    this.webTablesAddModalSalaryField.fillFieldWithNewText(
      testDataSingle.salary
    );

    this.webTablesAddModalDepartmentField.fillFieldWithNewText(
      testDataSingle.department
    );
    this.webTablesAddModalSubmitButton.clickButton();
    let numOfElementsEnd = await I.grabNumberOfVisibleElements(
      this.webTablesTableRecordsCountOnPage
    );
    assert(numOfElementsInitial + 1 === numOfElementsEnd);
  }

  submitRegistrationFormModal() {
    this.webTablesAddModalSubmitButton.clickButton();
    this.webTablesAddModalTitle.waitToHide();
  }

  async editTableRecord(testDataSingle: dataSingle) {
    let numOfElementsInitial = await I.grabNumberOfVisibleElements(
      this.webTablesTableRecordsCountOnPage
    );
    this.webTablesTableEditIcon.clickButton();
    this.webTablesAddModalTitle.waitForVisible();

    this.webTablesAddModalFirstNameField.addTextToField(
      testDataSingle.editText
    );
    this.webTablesAddModalLastNameField.addTextToField(testDataSingle.editText);
    this.webTablesAddModalEmailField.fillFieldWithNewText(testDataSingle.email);
    this.webTablesAddModalAgeField.addTextToField(testDataSingle.editText);
    this.webTablesAddModalSalaryField.addTextToField(testDataSingle.editText);

    this.webTablesAddModalDepartmentField.addTextToField(
      testDataSingle.editText
    );
    this.webTablesAddModalSubmitButton.clickButton();
    this.webTablesAddModalTitle.waitToHide();
    let numOfElementsEnd = await I.grabNumberOfVisibleElements(
      this.webTablesTableRecordsCountOnPage
    );
    assert(numOfElementsInitial === numOfElementsEnd);
  }

  async validateRegistrationModalFields(validationData: string) {
    let numOfElementsInitial = await I.grabNumberOfVisibleElements(
      this.webTablesTableRecordsCountOnPage
    );
    this.webTablesAddModalFirstNameField.fillFieldWithNewText(validationData);
    this.webTablesAddModalLastNameField.fillFieldWithNewText(validationData);
    this.webTablesAddModalEmailField.fillFieldWithNewText(validationData);
    this.webTablesAddModalAgeField.fillFieldWithNewText(validationData);
    this.webTablesAddModalSalaryField.fillFieldWithNewText(validationData);
    this.webTablesAddModalDepartmentField.fillFieldWithNewText(validationData);
    this.webTablesAddModalSubmitButton.clickButton();
    let numOfElementsEnd = await I.grabNumberOfVisibleElements(
      this.webTablesTableRecordsCountOnPage
    );
    assert(numOfElementsInitial === numOfElementsEnd);
  }

  async deleteTableRow() {
    let numOfElementsInitial = await I.grabNumberOfVisibleElements(
      this.webTablesTableRecordsCountOnPage
    );
    assert(numOfElementsInitial > 0);
    this.webTablesTableDeleteIcon.clickButton();
    let numOfElementsEnd = await I.grabNumberOfVisibleElements(
      this.webTablesTableRecordsCountOnPage
    );
    assert(numOfElementsInitial - 1 === numOfElementsEnd);
  }

  //!!!needs correction to use data type!!!
  async submitRegistrationFormBDD(
    firstName: string,
    lastName: string,
    email: string,
    age: number,
    salary: number,
    department: string
  ) {
    let numOfElementsInitial = await I.grabNumberOfVisibleElements(
      this.webTablesTableRecordsCountOnPage
    );
    this.webTablesAddModalFirstNameField.fillFieldWithNewText(firstName);
    this.webTablesAddModalLastNameField.fillFieldWithNewText(lastName);
    this.webTablesAddModalEmailField.fillFieldWithNewText(email);
    this.webTablesAddModalAgeField.fillFieldWithNewText(age);
    this.webTablesAddModalSalaryField.fillFieldWithNewText(salary);
    this.webTablesAddModalDepartmentField.fillFieldWithNewText(department);
    this.webTablesAddModalSubmitButton.clickButton();
    let numOfElementsEnd = await I.grabNumberOfVisibleElements(
      this.webTablesTableRecordsCountOnPage
    );
    assert(numOfElementsInitial + 1 === numOfElementsEnd);
  }
}

export = new WebTablesPage();
