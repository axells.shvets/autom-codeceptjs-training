import assert from 'assert';
import { BaseComponent } from '../../components/baseComponent';
import { DraggableComponent } from '../../components/draggableComponent';

const { I } = inject();

class ResizablePage {
  resizableCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Resizable")]'
  );
  resizableResizableBoxWithRestrictionHandler: DraggableComponent =
    new DraggableComponent(
      '#resizableBoxWithRestriction > span',
      'div.constraint-area'
    );
  resizableResizableBoxWithRestrictionTextElement: BaseComponent =
    new BaseComponent('#resizableBoxWithRestriction');
  resizableResizableBoxHandler: DraggableComponent = new DraggableComponent(
    '#resizable > span',
    '#RightSide_Advertisement'
  );
  resizableResizableBoxText: BaseComponent = new BaseComponent(
    '#resizable > div'
  );

  openResizableCategory() {
    this.resizableCategory.clickElement();
  }

  async resizeRestrictedResizableBox() {
    let originalSize =
      await this.resizableResizableBoxWithRestrictionTextElement.getElementWidth();
    this.resizableResizableBoxWithRestrictionHandler.dragAndDropElement();
    let finalSize =
      await this.resizableResizableBoxWithRestrictionTextElement.getElementWidth();
    assert(originalSize < finalSize);
  }

  async resizeFreeResizableBox() {
    let originalSize = await this.resizableResizableBoxText.getElementWidth();
    this.resizableResizableBoxHandler.dragAndDropElement();
    let finalSize = await this.resizableResizableBoxText.getElementWidth();
    assert(originalSize < finalSize);
  }
}

export = new ResizablePage();
