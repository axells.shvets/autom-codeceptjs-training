import { BaseComponent } from '../../components/baseComponent';
import { Button } from '../../components/buttonComponent';

class ButtonsPage {
  buttonsCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Buttons")]'
  );
  buttonsDoubleClickButton: Button = new Button('#doubleClickBtn');
  buttonsRightClickMeButton: Button = new Button('#rightClickBtn');
  buttonsClickMeButton: Button = new Button('//button[text()="Click Me"]');
  buttonsDoubleClickMessage: BaseComponent = new BaseComponent(
    '#doubleClickMessage'
  );
  buttonsRightClickMessage: BaseComponent = new BaseComponent(
    '#rightClickMessage'
  );
  buttonsClickMessage: BaseComponent = new BaseComponent(
    '#dynamicClickMessage'
  );

  openButtonsCategory() {
    this.buttonsCategory.clickElement();
  }

  clickAllButtonsPageButtons() {
    this.buttonsDoubleClickButton.doubleClickButton();
    this.buttonsDoubleClickMessage.waitForVisible();
    this.buttonsRightClickMeButton.rightClickButton();
    this.buttonsRightClickMessage.waitForVisible();
    this.buttonsClickMeButton.clickButton();
    this.buttonsClickMessage.waitForVisible();
  }
}

export = new ButtonsPage();
