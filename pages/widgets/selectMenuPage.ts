import { onClickDropDownComponent } from '../../components/onClickDropDownComponent';
import { BaseComponent } from '../../components/baseComponent';
import { DropDown } from '../../components/dropDownComponent';
import { EditBox } from '../../components/editBoxComponent';

const { I } = inject();

class SelectMenuPage {
  protected selectMenuMultipleDropDownText: any[] = [
    'Green',
    'Blue',
    'Black',
    'Red',
  ];

  selectMenuCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Select Menu")]'
  );
  selectMenuSelectValueField: onClickDropDownComponent =
    new onClickDropDownComponent(
      '#withOptGroup > div > div.css-1hwfws3',
      'div.css-26l3qy-menu',
      '#react-select-2-option-2'
    );
  selectMenuSelectOneField: onClickDropDownComponent =
    new onClickDropDownComponent(
      '#selectOne > div > div.css-1hwfws3',
      'div.css-26l3qy-menu'
    );
  selectMenuSelectOneFieldOptions: BaseComponent = new BaseComponent(
    '//div[@class=" css-yt9ioa-option"]'
  );
  selectMenuOldStileDropDown: DropDown = new DropDown(
    '#oldSelectMenu',
    ' > option'
  );
  selectMenuMultiSelectClickElement: EditBox = new EditBox(
    'div:nth-child(7) > div > div'
  );
  selectMenuStandardMultiSelect: DropDown = new DropDown('#cars', ' > option');

  openSelectMenuCategory() {
    this.selectMenuCategory.clickElement();
  }
  async validateAllSelectVariants() {
    this.selectMenuSelectValueField.selectInteractiveDropDownOption();
    this.selectMenuSelectOneField.clickElement();
    const interactiveDropDownOptions =
      await this.selectMenuSelectOneFieldOptions.getAllObjectsAsArray();
    const selectedText =
      await this.selectMenuSelectOneFieldOptions.grabTextFromAllElements();
    this.selectMenuSelectOneField.clickElement();
    for (let i = 0; i < interactiveDropDownOptions.length; i++) {
      this.selectMenuSelectOneField.selectInteractiveDropDownOption(
        interactiveDropDownOptions[i].getSelector()
      );
      this.selectMenuSelectOneField.waitForTextOnElement(selectedText[i]);
    }
    this.selectMenuOldStileDropDown.clickElement();
    let dropDownOptions =
      await this.selectMenuOldStileDropDown.getAllSelectOptionsText();
    for (let i = 0; i < dropDownOptions.length; i++) {
      this.selectMenuOldStileDropDown.selectSelectOption(dropDownOptions[i]);
    }
    for (let i = 0; i < this.selectMenuMultipleDropDownText.length; i++) {
      this.selectMenuMultiSelectClickElement.clickElement();
      this.selectMenuMultiSelectClickElement.addTextToField(
        this.selectMenuMultipleDropDownText[i]
      );
      I.pressKey('Enter');
    }
    let multiSelectOptions =
      await this.selectMenuStandardMultiSelect.getAllSelectOptionsText();
    for (let i = 0; i < multiSelectOptions.length; i++) {
      this.selectMenuStandardMultiSelect.selectSelectOption(
        multiSelectOptions[i]
      );
    }
  }
}

export = new SelectMenuPage();
