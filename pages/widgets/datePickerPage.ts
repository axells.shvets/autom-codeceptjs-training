import { BaseComponent } from '../../components/baseComponent';
import { DropDown } from '../../components/dropDownComponent';
import { EditBox } from '../../components/editBoxComponent';
import { dataSingleDateAndTime } from '../../data/dataProvider/testDataProviderTypes';

const { I } = inject();

class DatePickerPage {
  protected datePickerSelectDateAndTimeTimeSelect: string =
    '//li[@class="react-datepicker__time-list-item "]'; //please add exact date to locator -- [contains(text(), "${date}")]',
  protected datePickerSelectDateAndTimeMonthDropDownValues: string =
    'div.react-datepicker__month-dropdown > div:nth-child('; //please do not forget to add ")" after month number
  protected datePickerSelectDateAllDates: string =
    '[class*=react-datepicker__day--0'; //please do not forget to add "]" after day number

  datePickerCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Date Picker")]'
  );
  datePickerSelectDateField: EditBox = new EditBox('#datePickerMonthYearInput');
  datePickerSelectDateAndTimeField: EditBox = new EditBox(
    '#dateAndTimePickerInput'
  );
  datePickerSelectDateMonthsDropDown: DropDown = new DropDown(
    '.react-datepicker__month-dropdown-container--select > select'
  );
  datePickerSelectYearDropDown: DropDown = new DropDown(
    '.react-datepicker__year-dropdown-container--select > select'
  );
  datePickerSelectDateAndTimeMonthDropDown: DropDown = new DropDown(
    'span.react-datepicker__month-read-view--selected-month'
  );
  datePickedSelectDateAndTimeYearDropDown: DropDown = new DropDown(
    'span.react-datepicker__year-read-view--selected-year'
  );
  datePickerSelectDateAndTimeYearDropDownValue: BaseComponent =
    new BaseComponent('div.react-datepicker__year-dropdown > div:nth-child(8)');

  openDatePickerCategory() {
    this.datePickerCategory.clickElement();
  }

  selectDateAllDates(data: dataSingleDateAndTime) {
    for (let i = 0; i < data.daysList.length; i++) {
      this.datePickerSelectDateField.clickElement();
      if (i < data.monthsList.length) {
        this.datePickerSelectDateMonthsDropDown.selectSelectOption(
          data.monthsList[i]
        );
      }
      if (i < data.yearsList.length) {
        this.datePickerSelectYearDropDown.selectSelectOption(data.yearsList[i]);
      }
      I.click(this.datePickerSelectDateAllDates + data.daysList[i] + ']');
    }
  }

  selectDateAndTimeAllDates(data: dataSingleDateAndTime) {
    for (let i = 0; i < data.timeValuesList.length; i++) {
      this.datePickerSelectDateAndTimeField.clickElement();
      this.datePickerSelectDateAndTimeMonthDropDown.clickElement();
      I.waitForVisible(
        this.datePickerSelectDateAndTimeMonthDropDownValues + '3)'
      );
      I.click(this.datePickerSelectDateAndTimeMonthDropDownValues + '3)');
      this.datePickedSelectDateAndTimeYearDropDown.clickElement();
      this.datePickerSelectDateAndTimeYearDropDownValue.clickElement();

      if (i < data.daysList.length) {
        I.click(this.datePickerSelectDateAllDates + data.daysList[i] + ']');
      }
      I.click(
        this.datePickerSelectDateAndTimeTimeSelect +
          `[contains(text(), "${data.timeValuesList[i]}")]`
      );
    }
  }
}

export = new DatePickerPage();
