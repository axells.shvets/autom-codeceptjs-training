import { BaseComponent } from '../../components/baseComponent';

class AccordianPage {
  accordianCategory: BaseComponent = new BaseComponent(
    '//span[contains(text(),"Accordian")]'
  );
  accordianAllHeaders: BaseComponent = new BaseComponent(
    '//div[@class="card"]'
  );

  openAccordianCategory() {
    this.accordianCategory.clickElement();
  }

  async goOverAccordianSections() {
    let accordianHeaders =
      await this.accordianAllHeaders.getAllObjectsAsArray();
    for (let header of accordianHeaders) {
      header.clickElement();
    }
  }
}

export = new AccordianPage();
